package me.ford.fordenchants;

import java.io.File;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;

import org.bstats.bukkit.Metrics;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.plugin.java.JavaPluginLoader;

import me.ford.fordenchants.commands.EnchantCommand;
import me.ford.fordenchants.commands.ReloadCommand;
import me.ford.fordenchants.enchants.FordEnchant;
import me.ford.fordenchants.enchants.armor.FurnaceEnhanceEnchant;
import me.ford.fordenchants.enchants.armor.MagnetEnchant;
import me.ford.fordenchants.enchants.armor.PhantomRepellentEnchant;
import me.ford.fordenchants.enchants.armor.SpawnerEnhanceEnchant;
import me.ford.fordenchants.enchants.bow.DisarmEnchant;
import me.ford.fordenchants.enchants.bow.MarkEmEnchant;
import me.ford.fordenchants.enchants.crossbow.NeutralizeEnchant;
import me.ford.fordenchants.enchants.crossbow.TagEmEnchant;
import me.ford.fordenchants.enchants.tool.XRayEnchant;
import me.ford.fordenchants.enchants.weapon.DamageSpreadEnchant;
import me.ford.fordenchants.enchants.weapon.LeapEnchant;
import me.ford.fordenchants.enchants.weapon.LevitateEnchant;
import me.ford.fordenchants.enchants.weapon.PullEnchant;
import me.ford.fordenchants.enchants.weapon.SummonWolfEnchant;
import me.ford.fordenchants.enchants.weapon.WeakenEnchant;
import me.ford.fordenchants.listeners.AnvilListener;
import me.ford.fordenchants.listeners.EnchantListener;
import me.ford.fordenchants.listeners.EnchantingTableListener;
import me.ford.fordenchants.settings.Messages;
import me.ford.fordenchants.settings.Settings;
import me.ford.fordenchants.utils.Utils;

public class FordEnchants extends JavaPlugin {
	private final Set<FordEnchant> enchants = new HashSet<>();
	private final Set<String> enchantNames = new HashSet<>();
	private Random random;
	private Messages messages;
	private Settings settings;
	private final boolean amTesting;
	
	public FordEnchants() {
		super();
		amTesting = false;
	}
	
	protected FordEnchants(JavaPluginLoader loader, PluginDescriptionFile description, File dataFolder, File file) {
		super(loader, description, dataFolder, file);
		amTesting = true;
    }
	
	@SuppressWarnings("deprecation")
	@Override
	public void onEnable() {
		// config
		saveDefaultConfig();
		getConfig().options().copyDefaults(true);
		saveConfig();
		// settings
		settings = new Settings(this);

		// metrics
		if (!amTesting) new Metrics(this);
		
		// enable registering of new enchants
		if (!Utils.enableRegistration()) {
			getLogger().severe("Unable to add enchantments!");
			getLogger().severe("Disabling plugin!");
			getPluginLoader().disablePlugin(this);
			return;
		}
		
		// random
		random = new Random();
		
		// listener
		EnchantListener listener = new EnchantListener(this);
		getServer().getPluginManager().registerEvents(listener, this);
		AnvilListener anvilListener = new AnvilListener(this);
		getServer().getPluginManager().registerEvents(anvilListener, this);
		EnchantingTableListener tableListener = new EnchantingTableListener(this);
		getServer().getPluginManager().registerEvents(tableListener, this);
        
		// register enchants
		enchants.add(new PhantomRepellentEnchant(this));
		enchants.add(new SpawnerEnhanceEnchant(this));
		enchants.add(new SummonWolfEnchant(this));
		enchants.add(new DamageSpreadEnchant(this));
		enchants.add(new FurnaceEnhanceEnchant(this));
		enchants.add(new PullEnchant(this));
		enchants.add(new LevitateEnchant(this));
		enchants.add(new WeakenEnchant(this));
		enchants.add(new DisarmEnchant(this));
		enchants.add(new NeutralizeEnchant(this));
		enchants.add(new MarkEmEnchant(this));
		enchants.add(new TagEmEnchant(this));
		enchants.add(new LeapEnchant(this));
		enchants.add(new MagnetEnchant(this));
		enchants.add(new XRayEnchant(this));
		for (FordEnchant e : enchants) {
			Enchantment.registerEnchantment(e);
			listener.registerEnchant(e);
		}
		enchantNames.addAll(enchants.stream().map(FordEnchant::getName).collect(Collectors.toSet()));
		// disable registration
		if (!Utils.disableRegistration()) {
			getLogger().warning("Issue while disabling registration of new enchants!");
		}
		
		// register commands
		getCommand("fordenchant").setExecutor(new EnchantCommand(this));
		getCommand("fordenchantreload").setExecutor(new ReloadCommand(this));
		
		// messages
		messages = new Messages(this);
	}
	
	@Override
	public void onDisable() {
		for (FordEnchant enchant : enchants) {
			if (!Utils.unRegisterEnchant(enchant)) {
				getLogger().warning("Unable to unregister enchant: " + enchant);
			}
		}
	}
	
	public void reload() {
		reloadConfig();
	}
	
	public Set<String> getEnchantNames() {
		return enchantNames;
	}
	
	@SuppressWarnings("deprecation")
	public FordEnchant getEnchant(String name) {
		for (FordEnchant e : enchants) {
			if (e.getName().equalsIgnoreCase(name)) {
				return e;
			}
		}
		return null;
	}
	
	public Set<FordEnchant> getEnchants() {
		return new HashSet<>(enchants); // copy
	}
	
	public Random getRandomProvider() {
		return random;
	}
	
	public Messages getMessages() {
		return messages;
	}
	
	public Settings getSettings() {
		return settings;
	}

}
