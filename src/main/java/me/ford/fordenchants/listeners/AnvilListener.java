package me.ford.fordenchants.listeners;

import java.util.Map.Entry;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.PrepareAnvilEvent;
import org.bukkit.inventory.AnvilInventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.Repairable;
import org.bukkit.inventory.meta.Damageable;
import org.bukkit.inventory.meta.ItemMeta;

import me.ford.fordenchants.FordEnchants;
import me.ford.fordenchants.enchants.FordEnchant;
import me.ford.fordenchants.utils.ItemUtils;

public class AnvilListener implements Listener {
	private final FordEnchants FE;
	
	public AnvilListener(FordEnchants plugin) {
		FE = plugin;
	}
	
	@EventHandler
	public void onPrepareAnvilEvent(PrepareAnvilEvent event) {
		ItemStack result = event.getResult();
		InventoryHolder ih = event.getView().getBottomInventory().getHolder();
		AnvilInventory inv = event.getInventory();
		if (result == null || result.getType() == Material.AIR) {
			// try a tick later - maybe something was put in
			FE.getServer().getScheduler().runTask(FE, () -> {
				ItemStack res = getResult(inv);
				if (res == null || res.getType() == Material.AIR) {
					return; // nothing
				}
				AnvilPrepareResult apr = onPrepareAnvil(inv, res, ih, true); 
				if (apr.apply()) { 
					apr.applyTo(inv, true);
				}
				
			});
			return;
		}
		AnvilPrepareResult apr = onPrepareAnvil(inv, result, ih); 
		if (apr.apply()) { 
			apr.applyTo(inv);
		}
		
	}
	private AnvilPrepareResult onPrepareAnvil(AnvilInventory inv, ItemStack result, InventoryHolder ih) {
		return onPrepareAnvil(inv, result, ih, false);
	}
	
	private AnvilPrepareResult onPrepareAnvil(AnvilInventory inv, ItemStack result, InventoryHolder ih, boolean second) {
		ItemStack left = inv.getItem(0);
		ItemStack right = inv.getItem(1);
		if ((left == null || left.getType() == Material.AIR) || 
				(right == null || right.getType() == Material.AIR) ||
				!(left.getItemMeta() instanceof Repairable) ||
				left.getType() != right.getType()) {
			return new AnvilPrepareResult(false);
		}
		
		
		// override perms
		boolean levelOverRide = false;
		boolean addLevelFromEqual = FE.getSettings().addLevelWhenEqual();
		boolean simpleLevels = FE.getSettings().simpleAnvilLevels();
		if (ih instanceof Player) {
			levelOverRide = ((Player) ih).hasPermission("fordenchants.level-bypass");
		} else {
			FE.getLogger().warning("Could not figure out the player doing the enchanting because the inventory is not owned by a player!");
		}
		
		int total = 0;
		for (FordEnchant ench : FE.getEnchants()) {
			int lLevel = ItemUtils.getLevel(left, ench);
			int rLevel = ItemUtils.getLevel(right, ench);
			int level = Math.max(lLevel, rLevel); // max
			if (level > 0) { // TODO - incompatible enchants (there's none right now)
				if (lLevel == rLevel && addLevelFromEqual) level++;
				try {
					ItemUtils.addEnchant(result, ench, level, levelOverRide);
				} catch (IllegalArgumentException e) { // set max level
					ItemUtils.addEnchant(result, ench, ench.getMaxLevel(), levelOverRide);
				}
				total++;
			}
		}
		if (second) { // add enchants from right side 
			label: for (Entry<Enchantment, Integer> entry : right.getEnchantments().entrySet()) {
				if (entry.getKey() instanceof FordEnchant) continue; // skip already done ones
				for (Entry<Enchantment, Integer> entry2 : result.getEnchantments().entrySet()) {
					if (entry.getKey().conflictsWith(entry2.getKey()) ||
							entry2.getKey().conflictsWith(entry.getKey())) {
						continue label; // skip conflicting ones
					}
				}
				int curLevel = ItemUtils.getLevel(result, entry.getKey());
				int max = Math.max(curLevel, entry.getValue());
				if (max > curLevel) { // this also preserves higher than regular levels
					result.addEnchantment(entry.getKey(), max);
				} else if (entry.getValue() == curLevel && curLevel < entry.getKey().getMaxLevel()) {
					result.addEnchantment(entry.getKey(), curLevel + 1);
				}
			}
		}
		AnvilPrepareResult apr;
		if (total > 0) {
			boolean newName = hasNewName(inv, left);
			int repCost = getRepairCost(left, right, result, newName, simpleLevels);
			int resultRepairCost = Math.max(getRepairCost(left), getRepairCost(right)) * 2 + 1; // should mimic vanilla
			ItemMeta meta = result.getItemMeta();
			((Repairable) meta).setRepairCost(resultRepairCost);
			if (newName) meta.setDisplayName(inv.getRenameText());
			result.setItemMeta(meta);
			apr = new AnvilPrepareResult(true, result, repCost);
		} else {
			apr = new AnvilPrepareResult(false);
		}
		return apr;
	}
	
	private boolean hasNewName(AnvilInventory inv, ItemStack left) {
		String s = inv.getRenameText();
		if (s == null || s.isEmpty()) return false;
		String[] nameParts = left.getType().name().toLowerCase().split("_");
		String name = "";
		for (String part : nameParts) {
			part = part.substring(0, 1).toUpperCase() + part.substring(1);
			if (!name.isEmpty()) name += " ";
			name += part;
		}
		return !s.equals(name);
	}
	
	private int getRepairCost(ItemStack left, ItemStack right, ItemStack result, boolean newName, boolean simple) {
		int repCost = getRepairCost(left) + getRepairCost(right);
		
		if (simple) {
			return repCost;
		}
		int dam1 = ((Damageable) left.getItemMeta()).getDamage();
		int dam2 = ((Damageable) right.getItemMeta()).getDamage();
		
		// if new name, add 1
		if (newName) repCost ++;
		// if first is damaged, add 1
		if (dam1 > 0) repCost ++;
		// if second is damaged, add 2
		if (dam2 > 0) repCost += 2;
		// for every incompatible enchant on second item, add 1
		for (Entry<Enchantment, Integer> entry : left.getEnchantments().entrySet()) {
			for (Entry<Enchantment, Integer> entry2 : right.getEnchantments().entrySet()) {
				if (entry.getKey().conflictsWith(entry2.getKey())) {
					repCost += 1;
				}
			}
		}
		// add rarity * level for each enchant on right (with level of result)
		for (Entry<Enchantment, Integer> entry : right.getEnchantments().entrySet()) {
			repCost += ItemUtils.getLevel(result, entry.getKey()); // rarity 1
		}
		return repCost;
	}
	
	private int getRepairCost(ItemStack item) {
		ItemMeta meta = item.getItemMeta();
		if (meta instanceof Repairable) {
			int cost = ((Repairable) meta).getRepairCost();
			if (cost == 0) cost++;
			return cost;
		} else {
			return 0;
		}
	}
	
	private ItemStack getResult(AnvilInventory inv) { // now for when the first item only has custom enchantments (and is not damaged) and thus the "recipe" is not completed
		ItemStack res;
		ItemStack left = inv.getItem(0);
		ItemStack right = inv.getItem(1);

		if (left == null || right == null) return null;
		if (left.getType() != right.getType() || (left.getEnchantments().isEmpty() && right.getEnchantments().isEmpty())) {
			return null;
		}
		if (!(left.getItemMeta() instanceof Repairable)) return null; // not repairable
		if (((Damageable) left.getItemMeta()).hasDamage()) return null; // if left is damaged, it'll be automatic
		
		res = new ItemStack(left.getType());
		res.setItemMeta(left.getItemMeta());
		return res;
	}
	
	private class AnvilPrepareResult {
		private final boolean isCustom;
		private final ItemStack result;
		private final int repairCost;
		
		private AnvilPrepareResult(boolean isCustom) {
			this(isCustom, null, 0);
		}
		
		private AnvilPrepareResult(boolean isCustom, ItemStack result, int repairCost) {
			this.isCustom = isCustom;
			this.result = result;
			this.repairCost = repairCost;
		}
		
		private boolean apply() {
			return isCustom;
		}
		
		private ItemStack getResult() {
			return result;
		}
		
		private int getRepairCost() {
			return repairCost;
		}
		
		private void applyTo(AnvilInventory inv) {
			applyTo(inv, false);
		}
		
		private void applyTo(AnvilInventory inv, boolean now) {
			if (now) {
				inv.setRepairCost(getRepairCost());
			} else {
				FE.getServer().getScheduler().runTask(FE, () -> inv.setRepairCost(getRepairCost()));
			}
			inv.setItem(2, getResult());
		}
	}
	

}
