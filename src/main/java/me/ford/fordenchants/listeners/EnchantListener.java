package me.ford.fordenchants.listeners;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.EntityTargetLivingEntityEvent;
import org.bukkit.event.entity.SpawnerSpawnEvent;
import org.bukkit.event.inventory.FurnaceSmeltEvent;
import org.bukkit.event.player.PlayerChangedMainHandEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.server.PluginDisableEvent;

import me.ford.fordenchants.FordEnchants;
import me.ford.fordenchants.enchants.FordEnchant;
import me.ford.fordenchants.enchants.level.LevelDependence;
import me.ford.fordenchants.enchants.responses.BlockBreakResponse;
import me.ford.fordenchants.enchants.responses.EntityDamageByEntityResponse;
import me.ford.fordenchants.enchants.responses.EntityDeathResponse;
import me.ford.fordenchants.enchants.responses.EntityTargetLivingEntityResponse;
import me.ford.fordenchants.enchants.responses.FurnaceSmeltResponse;
import me.ford.fordenchants.enchants.responses.PlayerChangedMainHandResponse;
import me.ford.fordenchants.enchants.responses.PlayerInteractResponse;
import me.ford.fordenchants.enchants.responses.PlayerJoinResponse;
import me.ford.fordenchants.enchants.responses.PlayerMoveBlockResponse;
import me.ford.fordenchants.enchants.responses.PlayerMoveResponse;
import me.ford.fordenchants.enchants.responses.PlayerQuitResponse;
import me.ford.fordenchants.enchants.responses.PluginDisableResponse;
import me.ford.fordenchants.enchants.responses.Response;
import me.ford.fordenchants.enchants.responses.Response.ResponseType;
import me.ford.fordenchants.enchants.responses.SpawnerSpawnResponse;
import me.ford.fordenchants.enchants.responses.ZeroLevelResponse;

public class EnchantListener implements Listener {
	private final FordEnchants FE;
	private final List<FordEnchant> enchants = new ArrayList<>();
	private final List<EntityDamageByEntityResponse> entityDamageByEntityResponders = new ArrayList<>();
	private final List<EntityTargetLivingEntityResponse> entityTargetLivingEntityResponders = new ArrayList<>();
	private final List<PlayerJoinResponse> playerJoinResponders = new ArrayList<>();
	private final List<PlayerQuitResponse> playerQuitResponders = new ArrayList<>();
	private final List<PluginDisableResponse> pluginDisableResponders = new ArrayList<>();
	private final List<SpawnerSpawnResponse> spawnerSpawnResponders = new ArrayList<>();
	private final List<FurnaceSmeltResponse> furnaceSmeltResponders = new ArrayList<>();
	private final List<PlayerInteractResponse> playerInteractResponders = new ArrayList<>();
	private final List<EntityDeathResponse> entityDeathResponders = new ArrayList<>();
	private final List<BlockBreakResponse> blockBreakResponders = new ArrayList<>();
	private final List<PlayerMoveResponse> playerMoveResponders = new ArrayList<>();
	private final List<PlayerMoveBlockResponse> playerMoveBlockResponders = new ArrayList<>();
	private final List<PlayerChangedMainHandResponse> playerChangedMainHandResponders = new ArrayList<>();
	
	@Override
	public String toString() {
		return "EnchantListener with enchants: " + enchants + 
				"(entityDamageByEntityResponders:" + entityDamageByEntityResponders +
				", entityTargetLivingEntityResponders:" + entityTargetLivingEntityResponders +
				", playerJoinResponders:" + playerJoinResponders +
				", playerQuitResponders:" + playerQuitResponders +
				", PluginDisableResponse:" + pluginDisableResponders +
				", spawnerSpawnResponders:" + spawnerSpawnResponders +
				", furnaceSmeltResponders:" + furnaceSmeltResponders + 
        		", playerInteractResponders:" + playerInteractResponders + 
        		", entityDeathResponders:" + entityDeathResponders +
        		", blockBreakResponders:" + blockBreakResponders + ")";
	}
	
	public EnchantListener(FordEnchants plugin) {
		FE = plugin;
		
	}
	
	public int getLevel(Response ench, Event event) {
		int level = 0;
		if (ench instanceof LevelDependence) {
			level = ((LevelDependence) ench).getLevel(event);
		}
		return level;
	}
	
	public boolean ignoreZeroLevel(Response ench, Class<? extends Response> clazz) {
		if (ench instanceof ZeroLevelResponse && ((ZeroLevelResponse) ench).zeroLevelFor(clazz)) {
			return false; // override for certain responses
		}
		if (ench instanceof LevelDependence) {
			return ((LevelDependence) ench).ignoreZeroLevel();
		}
		return false; // if not level dependent, I'll have to go through with it anyway
	}
	
	public void registerEnchant(FordEnchant enchant) {
		enchants.add(enchant);
		
		if (enchant instanceof EntityDamageByEntityResponse) {
			entityDamageByEntityResponders.add((EntityDamageByEntityResponse) enchant);
		}
		if (enchant instanceof EntityTargetLivingEntityResponse) {
			entityTargetLivingEntityResponders.add((EntityTargetLivingEntityResponse) enchant);
		}
		if (enchant instanceof SpawnerSpawnResponse) {
			spawnerSpawnResponders.add((SpawnerSpawnResponse) enchant);
		}
		if (enchant instanceof PlayerJoinResponse) {
			playerJoinResponders.add((PlayerJoinResponse) enchant);
		}
		if (enchant instanceof PlayerQuitResponse) {
			playerQuitResponders.add((PlayerQuitResponse) enchant);
		}
		if (enchant instanceof PluginDisableResponse) {
			pluginDisableResponders.add((PluginDisableResponse) enchant);
		}
		if (enchant instanceof FurnaceSmeltResponse) {
			furnaceSmeltResponders.add((FurnaceSmeltResponse) enchant);
		}
		if (enchant instanceof PlayerInteractResponse) {
			playerInteractResponders.add((PlayerInteractResponse) enchant);
		}
		if (enchant instanceof EntityDeathResponse) {
			entityDeathResponders.add((EntityDeathResponse) enchant);
		}
		if (enchant instanceof BlockBreakResponse) {
			blockBreakResponders.add((BlockBreakResponse) enchant);
		}
		if (enchant instanceof PlayerMoveBlockResponse) {
			playerMoveBlockResponders.add((PlayerMoveBlockResponse) enchant);
		} else if (enchant instanceof PlayerMoveResponse) {
			playerMoveResponders.add((PlayerMoveResponse) enchant);
		}
		if (enchant instanceof PlayerChangedMainHandResponse) {
			playerChangedMainHandResponders.add((PlayerChangedMainHandResponse) enchant);
		}
	}
	
	public void handleEvent(Event event, List<? extends Response> responders, Class<? extends Response> clazz) {
		for (Response ench : responders) {
			int level = getLevel(ench, event);
			if (level == 0 && ignoreZeroLevel(ench, clazz)) continue;
			
			try {
				ench.onEvent(event, level);
			} catch (RuntimeException e) {
				FE.getLogger().severe("Issue while processing " + event  + " and " + ench);
				e.printStackTrace();
			}
		}
		
	}
	@EventHandler
	public void onEntityDamageByEntityEventWeaponDamager(EntityDamageByEntityEvent event) {		
		handleEvent(event, entityDamageByEntityResponders, ResponseType.getResponseClass(event.getClass()));
	}
	
	@EventHandler
	public void onEntityTargetLivingEntityEvent(EntityTargetLivingEntityEvent event) {
		handleEvent(event, entityTargetLivingEntityResponders, ResponseType.getResponseClass(event.getClass()));
	}
	
	@EventHandler
	public void onSpawnerSpawnEvent(SpawnerSpawnEvent event) {
		handleEvent(event, spawnerSpawnResponders, ResponseType.getResponseClass(event.getClass()));
	}
	
	@EventHandler
	public void onPlayerJoinEvent(PlayerJoinEvent event) {
		handleEvent(event, playerJoinResponders, ResponseType.getResponseClass(event.getClass()));
	}
	
	@EventHandler
	public void onPlayerQuitEvent(PlayerQuitEvent event) {
		handleEvent(event, playerQuitResponders, ResponseType.getResponseClass(event.getClass()));
	}
	
	@EventHandler
	public void onPluginDisableEvent(PluginDisableEvent event) {
		handleEvent(event, pluginDisableResponders, ResponseType.getResponseClass(event.getClass()));
	}
	
	@EventHandler
	public void onFurnaceSmeltEvent(FurnaceSmeltEvent event) {
		handleEvent(event, furnaceSmeltResponders, ResponseType.getResponseClass(event.getClass()));
	}
	
	@EventHandler
	public void onPlayerInteractEvent(PlayerInteractEvent event) {
		handleEvent(event, playerInteractResponders, ResponseType.getResponseClass(event.getClass()));
	}
	
	@EventHandler
	public void onEntityDeathEvent(EntityDeathEvent event) {
		handleEvent(event, entityDeathResponders, ResponseType.getResponseClass(event.getClass()));
	}
	
	@EventHandler
	public void onBlockBreak(BlockBreakEvent event) {
		handleEvent(event, blockBreakResponders, ResponseType.getResponseClass(event.getClass()));
	}

	@EventHandler
	public void onPlayerMove(PlayerMoveEvent event) {
		handleEvent(event, playerMoveResponders, ResponseType.getResponseClass(event.getClass()));
		if (!playerMoveBlockResponders.isEmpty() && 
						event.getFrom().getBlock() != event.getTo().getBlock()) {
				handleEvent(event, playerMoveBlockResponders, ResponseType.getResponseClass(event.getClass()));
		}
	}

	@EventHandler
	public void onPlayerChangedMain(PlayerChangedMainHandEvent event) {
		handleEvent(event, playerChangedMainHandResponders, ResponseType.getResponseClass(event.getClass()));
	}

}
