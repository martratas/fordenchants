package me.ford.fordenchants.listeners;

import java.util.HashSet;
import java.util.Set;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.enchantment.EnchantItemEvent;
import org.bukkit.inventory.ItemStack;

import me.ford.fordenchants.FordEnchants;
import me.ford.fordenchants.enchants.FordEnchant;
import me.ford.fordenchants.utils.ItemUtils;

public class EnchantingTableListener implements Listener {
	private final FordEnchants FE;
	
	public EnchantingTableListener(FordEnchants plugin) {
		FE = plugin;
	}
	
	@EventHandler
	public void onEnchantItem(EnchantItemEvent event) {
		if (!FE.getSettings().useEnchantingTable()) return;
		ItemStack item = event.getItem();
		
		int enchantingLevel = event.getExpLevelCost();
		int nrOfCustoms = FE.getSettings().getNumberOfCustomEnchantsForLevel(enchantingLevel);
		if (nrOfCustoms <= 0) return;
		
		Set<FordEnchant> added = new HashSet<>();
		for (int i = 0; i < nrOfCustoms; i++) {
			double rnd = FE.getRandomProvider().nextDouble();
			FordEnchant ench = chooseEnchant(item, rnd, added);
			if (ench == null) {// no suitable enchants found
				FE.getLogger().info("No suitable custom enchants found for: " + item + "(wanted " + nrOfCustoms + ", got " + added.size() + ")");
				return;
			}
			int level = FE.getSettings().getLevelForCustomEnchantForLevel(enchantingLevel, ench.getMaxLevel());
			ItemUtils.addEnchant(item, ench, level, false);
		}
	}
	
	private FordEnchant chooseEnchant(ItemStack item, double rnd, Set<FordEnchant> ignore) { // if no suitable, null
		Set<FordEnchant> suitables = new HashSet<>();
		double totalChance = 0;
		for (FordEnchant ench : FE.getEnchants()) {
			if (!ench.canEnchantItem(item)) {
				continue;
			}
			if (ignore.contains(ench)) {
				continue;
			}
			suitables.add(ench);
			totalChance += ench.getSettings().chanceInTable();
		}
		double curChance = 0;
		
		rnd *= totalChance;
		for (FordEnchant ench : suitables) {
			double newChance = curChance + ench.getSettings().chanceInTable();
			if (curChance < rnd && rnd < newChance) {
				return ench;
			}
			curChance += ench.getSettings().chanceInTable();
		}
		return null; // if no suitables
	}

}
