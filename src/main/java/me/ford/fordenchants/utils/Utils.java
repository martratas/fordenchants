package me.ford.fordenchants.utils;

import java.lang.reflect.Field;
import java.text.NumberFormat;
import java.util.Map;
import java.util.function.Predicate;

import org.bukkit.Location;
import org.bukkit.NamespacedKey;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Entity;
import org.bukkit.util.RayTraceResult;
import org.bukkit.util.Vector;

import me.ford.fordenchants.enchants.FordEnchant;

public final class Utils {
	private static final String NPC = "NPC";
	private Utils() {}
	
	public static boolean enableRegistration() {
		Field acceptingNewField;
		try {
			acceptingNewField = Enchantment.class.getDeclaredField("acceptingNew");
		} catch (NoSuchFieldException | SecurityException e) {
			e.printStackTrace();
			return false;
		}
        acceptingNewField.setAccessible(true);
        try {
			acceptingNewField.set(null, true);
		} catch (IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
			return false;
		}
        return true;
	}
	
	public static boolean disableRegistration() {
		Field acceptingNewField;
		try {
			acceptingNewField = Enchantment.class.getDeclaredField("acceptingNew");
		} catch (NoSuchFieldException | SecurityException e) {
			e.printStackTrace();
			return false;
		}
        acceptingNewField.setAccessible(true);
        try {
			acceptingNewField.set(null, false);
		} catch (IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
			return false;
		}
        acceptingNewField.setAccessible(false);
        return true;
	}

	@SuppressWarnings({ "unchecked", "deprecation" })
	public static boolean unRegisterEnchant(FordEnchant ench) {
		Field byKeyField;
		try {
			byKeyField = Enchantment.class.getDeclaredField("byKey");
		} catch (NoSuchFieldException e) {
			e.printStackTrace();
			return false;
		}
		Field byNameField;
		try {
			byNameField = Enchantment.class.getDeclaredField("byName");
		} catch (NoSuchFieldException e) {
			e.printStackTrace();
			return false;
		}
		byKeyField.setAccessible(true);
		byNameField.setAccessible(true);
		
		Map<NamespacedKey, Enchantment> byKey;
		try {
			byKey = (Map<NamespacedKey, Enchantment>) byKeyField.get(null);
		} catch (IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
			return false;
		}
		Map<String, Enchantment> byName;
		try {
			byName = (Map<String, Enchantment>) byNameField.get(null);
		} catch (IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
			return false;
		}
		byKey.remove(ench.getKey());
		byName.remove(ench.getName());
		
		byKeyField.setAccessible(false);
		byNameField.setAccessible(false);
		return true;
	}
	
	public static final String[] levels = {"", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X"};
	public static String getRoman(int nr) {
		if (nr >= 0 && nr <= 10) {
			return levels[nr];
		}
		return "level." + nr;
	}
	
	public static Entity rayTrace(Location start, Vector direction, double distance, double skip, Predicate<Entity> pred) {
		// TODO - don't find a target behind blocks.
		start = start.add(direction.multiply(skip));
		RayTraceResult res = start.getWorld().rayTraceEntities(start, direction, distance - skip, pred);
		if (res == null) return null;
		return res.getHitEntity();
	}
	
	public static String formatDouble(double nr, int decimals) {
		final NumberFormat nf = NumberFormat.getInstance();
        nf.setMaximumFractionDigits(decimals);
        nf.setMinimumFractionDigits(0);
        return nf.format(nr);
	}
	
	public static boolean isNPC(Entity entity) {
		return entity == null ? false : entity.hasMetadata(NPC);
	}

}
