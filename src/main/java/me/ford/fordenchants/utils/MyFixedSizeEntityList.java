package me.ford.fordenchants.utils;

import java.util.ArrayList;

import org.bukkit.entity.LivingEntity;

public class MyFixedSizeEntityList extends ArrayList<LivingEntity> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5871336978817083318L;
	private final int size;
	
	public MyFixedSizeEntityList(int size) {
		super(size);
		for (int i = 0; i < size; i++) {
			add(null);
		}
		this.size = size;
	}
	
	@Override
	public boolean isEmpty() {
		for (LivingEntity e : this) {
			if (e != null) return false;
		}
		return true;
	}
	
	public boolean putAndPush(int position, LivingEntity entity) {
		if (position >= size) {
			return false;
		}
		shiftRight(position);
		set(position, entity);
		resize();
		return true;
	}
	
	public void shiftRight(int from) {
	    //make temp variable to hold last element
		LivingEntity last = get(size()-1); 

	    //make a loop to run through the array list
	    for(int i = size()-1; i > from; i--)
	    {
	        //set the last element to the value of the 2nd to last element
	        set(i, get(i-1)); 
	    }
	    //set the first element to be the last element
	    add(last); 
	}
	
	private void resize() {
		while (size() > size) {
			remove(size() - 1);
		}
	}

}
