package me.ford.fordenchants.utils;

import org.bukkit.entity.LivingEntity;

public class NoEquipmentException extends IllegalArgumentException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7393885104329725653L;
	
	public NoEquipmentException() {
		super("Entity has no equipment!");
	}
	
	public NoEquipmentException(LivingEntity ent) {
		super("Entity of type " + ent.getType() + " has no equipment!");
	}

}
