package me.ford.fordenchants.utils;

import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import me.ford.fordenchants.FordEnchants;
import me.ford.fordenchants.enchants.cooldowns.CooldownHandler;
import me.ford.fordenchants.settings.enchants.MarkEmTagEmBaseSettings;

public final class TagEmMarkEmUtils {
	
	private TagEmMarkEmUtils() { }
	
	public static void onEntityDamageByEntity(EntityDamageByEntityEvent event, int level, 
			CooldownHandler cooldown, MarkEmTagEmBaseSettings settings, FordEnchants plugin) {
		if (!(event.getEntity() instanceof LivingEntity)) return;
		LivingEntity beingDamaged = (LivingEntity) event.getEntity();
		Entity damager = DisarmNeutralizeUtils.getProjectileSource((EntityDamageByEntityEvent) event);
		if (cooldown.isOnCooldown(damager)) {
			return;
		}
		// chance
		double chance = settings.defaultChance() + level * settings.chancePerLevel();
		double nr = plugin.getRandomProvider().nextDouble();
		if (chance <= nr) {
			return;
		}
		
		if (!settings.affectPlayers() && beingDamaged instanceof Player && !Utils.isNPC(beingDamaged)) {
			return; // NPCs work like other mobs
		}
		int durationTicks = (int) Math.floor((settings.defaultDuration() + level * settings.durationPerLevel()) * 20);
		mark(beingDamaged, durationTicks);
		cooldown.startCooldown(damager);
		if (settings.sendMessage()) {
			damager.sendMessage(settings.getMessage(plugin.getMessages(), level));
		}
	}
	
	public static void mark(LivingEntity entity, int durationTicks) {
		entity.addPotionEffect(new PotionEffect(PotionEffectType.GLOWING, durationTicks, 1));
	}

}
