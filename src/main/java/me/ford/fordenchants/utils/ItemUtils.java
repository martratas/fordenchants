package me.ford.fordenchants.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.inventory.EntityEquipment;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.Damageable;
import org.bukkit.inventory.meta.ItemMeta;

import me.ford.fordenchants.enchants.FordEnchant;

public final class ItemUtils {
	
	private ItemUtils() { }
	
	@SuppressWarnings("deprecation")
	public static void addEnchant(ItemStack item, FordEnchant en, int level, boolean levelOverRide) throws IllegalArgumentException {
		ItemMeta meta = item.getItemMeta();
		if (level != 0) {
			if (!levelOverRide && (level < 0 || level > en.getMaxLevel())) {
				throw new IllegalArgumentException("Enchantment level must be between 0 and " + en.getMaxLevel() + ", got:" + level);
			}
			if (!levelOverRide && !en.canEnchantItem(item)) {
				throw new IllegalArgumentException("Cannot enchant item of type " + item.getType() + " with " + en.getName());
			}
			meta.addEnchant(en, level, true);
		} else {
			meta.removeEnchant(en);
		}
		item.setItemMeta(meta);
		// add lore
		String displayName = en.getDisplayName();
		removeLoreEnchant(item, displayName); // remove previous if present
		if (level > 0) {
			if (en.getMaxLevel() > 1) {
				displayName += " " + Utils.getRoman(level);
			}
			addLoreEnchant(item, displayName, false);
		}
	}
	
	public static void addLoreEnchant(ItemStack item, String displayName, boolean prev) { // TODO add functionality of upgrading
		if (item == null) return;
		ItemMeta meta = item.getItemMeta();
		List<String> lore = new ArrayList<>();
		if (meta.hasLore()) {
			lore = meta.getLore();
		}
		lore.add(0, displayName);
		meta.setLore(lore);
		item.setItemMeta(meta);
	}
	
	public static void removeLoreEnchant(ItemStack item, String displayName) {
		if (item == null) return;
		ItemMeta meta = item.getItemMeta();
		if (!meta.hasLore()) return;
		List<String> lore = meta.getLore();
		if (lore == null) return;
		lore.removeIf((line) -> line.startsWith(displayName));
		meta.setLore(lore);
		item.setItemMeta(meta);
	}
	
	// methods to determine type of item
	
	public static boolean isHelmet(ItemStack item) {
		return item == null ? false : isHelmetTypeName(item.getType().name());
	}
	
	public static boolean isHelmetTypeName(String name) {
		return name.contains("HELMET");
	}
	
	public static boolean isChestPlate(ItemStack item) {
		return item == null ? false : isChestPlateTypeName(item.getType().name());
	}
	
	public static boolean isChestPlateTypeName(String name) {
		return name.contains("CHESTPLATE");
	}
	
	public static boolean isLeggings(ItemStack item) {
		return item == null ? false : isLeggingsTypeName(item.getType().name());
	}
	
	public static boolean isLeggingsTypeName(String name) {
		return name.contains("LEGGINGS");
	}
	
	public static boolean isBoots(ItemStack item) {
		return item == null ? false : isBootsTypeName(item.getType().name());
	}
	
	public static boolean isBootsTypeName(String name) {
		return name.contains("BOOTS");
	}
	
	public static boolean isArmor(ItemStack item) {
		if (item == null) return false;
		String name = item.getType().name();
		return isHelmetTypeName(name) || isChestPlateTypeName(name) || isLeggingsTypeName(name) || isBootsTypeName(name);
	}
	
	public static boolean isTool(ItemStack item) {
		return item == null ? false : (isAxe(item) || isShovel(item) || isPickaxe(item)); // what about hoe?
	}
	
	public static boolean isSword(ItemStack item) {
		return (item == null) ? false : item.getType().name().contains("_SWORD");
	}
	
	public static boolean isAxe(ItemStack item) {
		return (item == null) ? false : item.getType().name().contains("_AXE");
	}

	public static boolean isShovel(ItemStack item) {
		return (item == null) ? false: item.getType().name().endsWith("_SHOVEL");
	}

	public static boolean isPickaxe(ItemStack item) {
		return (item == null) ? false: item.getType().name().endsWith("_PICKAXE");
	}
	
	public static boolean isWeapon(ItemStack item) {
		return isSword(item) || isAxe(item);
	}
	
	public static boolean isBowOrCrossbow(ItemStack item) {
		return isBow(item) || isCrossbow(item);
	}
	
	public static boolean isBow(ItemStack item) {
		return (item == null) ? false : item.getType() == Material.BOW;
	}
	
	public static boolean isCrossbow(ItemStack item) {
		try {
			Material.valueOf("CROSSBOW");
		} catch (IllegalArgumentException e) { // thrown when there's no crossbow (below 1.14)
			return false;
		}
		return (item == null) ? false : item.getType() == Material.CROSSBOW;
	}

	public static int getLevel(ItemStack item, Enchantment enchant) {
		if (!isGood(item)) return 0;
		Integer i = item.getEnchantments().get(enchant);
		return i == null ? 0 : i;
	}
	
	public static boolean isGood(ItemStack item) {
		return item != null && item.getType() != Material.AIR && item.hasItemMeta();
	}
	
	// methods to do with entities in an area
	
	public static int getTotalArmorLevelInArea(Location loc, double distance, FordEnchant enchant) {
		int levels = 0;
		for (Entry<LivingEntity, Map<EquipmentSlot, Integer>> entry1 : getArmorLevelOfEntitiesInArea(loc, distance, enchant).entrySet()) {
			for (Entry<EquipmentSlot, Integer> entry2 : entry1.getValue().entrySet()) {
				levels += entry2.getValue();
			}
		}
		return levels;
	}
	
	public static Map<LivingEntity, Map<EquipmentSlot, Integer>> getArmorLevelOfEntitiesInArea(Location loc, double distance, FordEnchant enchant) {
		Map<LivingEntity, Map<EquipmentSlot, Integer>> map = new HashMap<>();
		for (Entity e : loc.getWorld().getNearbyEntities(loc, distance, distance, distance, (e) -> e instanceof LivingEntity)) {
			Map<EquipmentSlot, Integer> cMap = getArmorLevel((LivingEntity) e, enchant);
			if (!cMap.isEmpty()) {
				map.put((LivingEntity) e, cMap);
			}
		}
		return map;
	}
	
	public static int getTotalArmorLevelOf(Entity entity, FordEnchant enchant) {
		if (!(entity instanceof LivingEntity)) return 0;
		int level = 0;
		EntityEquipment eq = ((LivingEntity) entity).getEquipment();
		if (eq == null) return 0;
		for (ItemStack armor : eq.getArmorContents()) {
			level += getLevel(armor, enchant);
		}
		return level;
	}

	public static int getMainHandLevelOf(Entity entity, FordEnchant enchant) {
		if (!(entity instanceof LivingEntity)) return 0;
		EntityEquipment eq = ((LivingEntity) entity).getEquipment();
		if (eq == null) return 0;
		return getLevel(eq.getItemInMainHand(), enchant);
	}
	
	// durabilities
	
	public static void handleDurabilityOfEntity(LivingEntity entity, Map<EquipmentSlot, Integer> equipment, FordEnchant enchant,
													int def, int perLevel, int max) {
		int tally = 0;
		EntityEquipment eq = entity.getEquipment();
		if (eq == null) throw new NoEquipmentException(entity);
		for (Entry<EquipmentSlot, Integer> entry : equipment.entrySet()) {
			EquipmentSlot slot = entry.getKey();
			ItemStack item;
			switch (slot) {
			case HEAD:
				item = eq.getHelmet();
				break;
			case CHEST:
				item = eq.getChestplate();
				break;
			case LEGS:
				item = eq.getLeggings();
				break;
			case FEET:
				item = eq.getBoots();
				break;
			default:
				item = null;
			}
			int cur = def + perLevel * entry.getValue();
			tally += cur;
			reduceDurability(item, cur);
			if (item != null && isBroken(item)) {
				item = null;
			}
			switch (slot) {
			case HEAD:
				eq.setHelmet(item);
				break;
			case CHEST:
				eq.setChestplate(item);
				break;
			case LEGS:
				eq.setLeggings(item);
				break;
			case FEET:
				eq.setBoots(item);
				break;
			default:
			}
			if (tally >= max) return;
		}
	}
	
	public static void handleDurabilityInArea(Location loc, int totalLevel, double distance, FordEnchant enchant, int def, int perLevel, int maxPerEntity) {
		for (Entry<LivingEntity, Map<EquipmentSlot, Integer>> entry : getArmorLevelOfEntitiesInArea(loc, distance, enchant).entrySet()) {
			handleDurabilityOfEntity(entry.getKey(), entry.getValue(), enchant, def, perLevel, maxPerEntity);
		}
	}
	
	public static void reduceDurability(ItemStack item, int nr) {
		if (item == null) return; // shouldn't hapen
		ItemMeta meta = item.getItemMeta();
		if (meta.isUnbreakable()) return; // unbreakable
		if (!(meta instanceof Damageable)) return; // could happen
		Damageable dam = (Damageable) meta;
		int newDam = dam.getDamage() + nr;
		if (newDam > item.getType().getMaxDurability()) {
			dam.setDamage(item.getType().getMaxDurability());
		} else {
			dam.setDamage(dam.getDamage() +  nr);
		}
		item.setItemMeta(meta);
	}
	
	public static void handleBowDurability(Entity ent, int nr) {
		handleBowCrossbowDurability(ent, nr, true);
	}
	
	public static void handleCrossbowDurability(Entity ent, int nr) {
		handleBowCrossbowDurability(ent, nr, false);
	}
	
	public static void handleBowCrossbowDurability(Entity ent, int nr, boolean bow) {
		if (ent instanceof LivingEntity) {
			EntityEquipment eq = ((LivingEntity) ent).getEquipment();
			if (eq == null) throw new NoEquipmentException((LivingEntity) ent);
			ItemStack main = eq.getItemInMainHand();
			ItemStack off = eq.getItemInOffHand();
			boolean bMain = bow ? isBow(main) : isCrossbow(main);
			if (bMain) {
				reduceDurability(main, nr);
				if (isBroken(main)) {
					eq.setItemInMainHand(null);
				} else {
					eq.setItemInMainHand(main);
				}
			} else {
				reduceDurability(off, nr);
				if (isBroken(off)) {
					eq.setItemInOffHand(null);
				} else {
					eq.setItemInOffHand(off);
				}
			}
		}
	}

	public static void handleMainHandDurability(Entity damager, int i) {
		if (damager instanceof LivingEntity) {
			EntityEquipment eq = ((LivingEntity) damager).getEquipment();
			if (eq == null) throw new NoEquipmentException((LivingEntity) damager);
			ItemStack item = eq.getItemInMainHand();
			reduceDurability(item, i);
			if (isBroken(item)) {
				eq.setItemInMainHand(null);
			} else {
				eq.setItemInMainHand(item);
			}
		}
		
	}

	public static boolean isBroken(ItemStack item) {
		ItemMeta meta = item.getItemMeta();
		if (!(meta instanceof Damageable)) return false;
		if (item.getType().getMaxDurability() == 0) return false;
		return ((Damageable) meta).getDamage() >= item.getType().getMaxDurability();
	}
	
	// methods to do with entities and their armor/tools/weapons
	
	public static Map<EquipmentSlot, Integer> getArmorLevel(Entity iEntity, FordEnchant enchant) {
		Map<EquipmentSlot, Integer> map = new HashMap<>();
		if (!(iEntity instanceof LivingEntity)) return map;
		LivingEntity entity = (LivingEntity) iEntity;
		int level = 0;
		EntityEquipment eq = entity.getEquipment();
		if (eq == null) return map;
		level = getLevel(eq.getHelmet(), enchant);
		if (level != 0) {
			map.put(EquipmentSlot.HEAD, level);
		}
		level = getLevel(eq.getChestplate(), enchant);
		if (level != 0) {
			map.put(EquipmentSlot.CHEST, level);
		}
		level = getLevel(eq.getLeggings(), enchant);
		if (level != 0) {
			map.put(EquipmentSlot.LEGS, level);
		}
		level = getLevel(eq.getBoots(), enchant);
		if (level != 0) {
			map.put(EquipmentSlot.FEET, level);
		}
		return map;
	}
	
	public static int getHelmetLevel(Entity entity, FordEnchant enchant) {
		return getLevel(getHelmet(entity), enchant);
	}
	
	public static int getMainHandLevel(Entity entity, FordEnchant enchant) {
		return getLevel(getItemInMainHand(entity), enchant);
	}

	public static int getBowLevel(Entity entity, FordEnchant enchant) {
		ItemStack item = getItemInMainHand(entity);
		if (!isGood(item) || !isBow(item)) {
			item = getItemInOffHand(entity);
			if (!isGood(item) || !isBow(item)) {
				return 0;
			}
		}
		return getLevel(item, enchant);
	}

	public static int getCrossbowLevel(Entity entity, FordEnchant enchant) {
		ItemStack item = getItemInMainHand(entity);
		if (!isGood(item) || !isCrossbow(item)) {
			item = getItemInOffHand(entity);
			if (!isGood(item) || !isCrossbow(item)) {
				return 0;
			}
		}
		return getLevel(item, enchant);
	}
	
	public static ItemStack getItemInMainHand(Entity entity) {
		if (!(entity instanceof LivingEntity)) return null;
		EntityEquipment eq = ((LivingEntity) entity).getEquipment();
		if (eq == null) return null;
		return eq.getItemInMainHand();
	}
	
	public static ItemStack getItemInOffHand(Entity entity) {
		if (!(entity instanceof LivingEntity)) return null;
		EntityEquipment eq = ((LivingEntity) entity).getEquipment();
		if (eq == null) return null;
		return eq.getItemInOffHand();
	}
	
	public static ItemStack getHelmet(Entity entity) {
		if (!(entity instanceof LivingEntity)) return null;
		EntityEquipment eq = ((LivingEntity) entity).getEquipment();
		if (eq == null) return null;
		return eq.getHelmet();
	}

}
