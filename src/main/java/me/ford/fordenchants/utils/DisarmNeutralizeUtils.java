package me.ford.fordenchants.utils;

import java.util.Map;
import java.util.logging.Logger;

import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.inventory.EntityEquipment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.projectiles.ProjectileSource;

import me.ford.fordenchants.FordEnchants;
import me.ford.fordenchants.enchants.cooldowns.CooldownHandler;
import me.ford.fordenchants.settings.enchants.DisarmNeutralizeBaseSettings;

public final class DisarmNeutralizeUtils {
	
	private DisarmNeutralizeUtils() { }
	
	public static ItemStack disarm(final LivingEntity entity) {
		final ItemStack main = ItemUtils.getItemInMainHand(entity);
		if (main == null || main.getType() == Material.AIR) return null;
		EntityEquipment eq = entity.getEquipment();
		if (eq == null) return null;
		eq.setItemInMainHand(null);
		return main;
	}
	
	public static void rearm(final LivingEntity entity, final ItemStack main, Logger logger) {
		if (entity.isValid() && !entity.isDead()) {
			EntityEquipment eq = entity.getEquipment();
			if (eq == null) throw new NoEquipmentException(entity);
			eq.setItemInMainHand(main);
		} else {
			logger.warning("Disarmed/Neutralized entity '" + entity + "' died while disarmed. Weapon:" + main);
		}
	}
	
	public static int bestIndex(PlayerInventory inv) {
		int curItem = inv.getHeldItemSlot();
		int first = inv.firstEmpty();
		
		if (first != curItem) {
			return first;
		}
		for (int i = 35; i > 8; i--) { // within inventory
			ItemStack cur = inv.getItem(i);
			if (cur == null || cur.getType() == Material.AIR) {
				return i;
			}
		}
		return first; // TODO - if full, it'll go to the same slot
	}
	
	public static void onEntityDamageByEntity(EntityDamageByEntityEvent event, int level,
			CooldownHandler cooldown, DisarmNeutralizeBaseSettings settings, FordEnchants plugin, Map<LivingEntity, ItemStack> disarmed) {
		if (!(event.getEntity() instanceof LivingEntity)) return;
		LivingEntity beingDamaged = (LivingEntity) event.getEntity();
		Entity damager = getProjectileSource(event);

		if (cooldown.isOnCooldown(damager)) {
			return;
		}
		// chance
		double chance = settings.defaultChance() + level * settings.chancePerLevel();
		double nr = plugin.getRandomProvider().nextDouble();
		if (chance <= nr) {
			return;
		}
		
		if (!settings.affectPlayers() && beingDamaged instanceof Player && !Utils.isNPC(beingDamaged)) {
			return; // NPCs work like other mobs
		}
		
		int delayTicks = (int) Math.floor(settings.giveBackAfter(level) * 20);
		ItemStack item = disarm(beingDamaged); 
		if (item == null) { // could not disarm
			return;
		}
		if (settings.putInInventoryForPlayers() && beingDamaged instanceof Player && !Utils.isNPC(beingDamaged)) {
			PlayerInventory inv = ((Player) beingDamaged).getInventory();
			int index = bestIndex(inv);
			inv.setItem(index, item);
		} else if (settings.throwOnGround()) {
			beingDamaged.getWorld().dropItemNaturally(beingDamaged.getLocation(), item);
		} else {
			disarmed.put(beingDamaged, item);
			plugin.getServer().getScheduler().runTaskLater(plugin, () -> {
				if (disarmed.containsKey(beingDamaged) && beingDamaged.isValid() && !beingDamaged.isDead()) {
					rearm(beingDamaged, disarmed.get(beingDamaged), plugin.getLogger());
					disarmed.remove(beingDamaged);
				}
			}, delayTicks);
		}
		cooldown.startCooldown(damager);
		if (settings.sendMessage()) {
			damager.sendMessage(settings.getMessage(plugin.getMessages(), level));
		}
	}
	
	public static void onEntityDeath(EntityDeathEvent event, DisarmNeutralizeBaseSettings settings, FordEnchants plugin, Map<LivingEntity, ItemStack> disarmed) {
		LivingEntity entity = event.getEntity();
		ItemStack stack = disarmed.remove(entity);
		if (stack != null) {
			// chance
			double chance = settings.chanceOfDroppingWhileDisarmed(entity.getType());
			double nr = plugin.getRandomProvider().nextDouble();
			if (chance <= nr) {
				return;
			}
			event.getDrops().add(stack);
		}
	}
	
	public static Entity getProjectileSource(EntityDamageByEntityEvent event) {
		Entity projectile = event.getDamager();
		if (projectile instanceof Projectile) {
			ProjectileSource source = ((Projectile) projectile).getShooter();
			if (source instanceof Entity) {
				return (Entity) source;
			}
		}
		return null;
	}

}
