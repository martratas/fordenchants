package me.ford.fordenchants.commands;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;

import me.ford.fordenchants.FordEnchants;

public class ReloadCommand implements TabExecutor {
	private final FordEnchants FE;
	
	public ReloadCommand(FordEnchants plugin) {
		FE = plugin;
	}
	
	
	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
		return new ArrayList<>();
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		FE.reload();
		sender.sendMessage(FE.getMessages().reloadedMessage());
		return true;
	}

}
