package me.ford.fordenchants.commands;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.StringUtil;

import me.ford.fordenchants.FordEnchants;
import me.ford.fordenchants.enchants.FordEnchant;
import me.ford.fordenchants.utils.ItemUtils;

public class EnchantCommand implements TabExecutor {
	private final FordEnchants FE;
	
	public EnchantCommand(FordEnchants plugin) {
		FE = plugin;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
		List<String> list = new ArrayList<>();
		if (args.length == 1) {
			return StringUtil.copyPartialMatches(args[0], FE.getEnchantNames(), list);
		}
		return list;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (args.length < 1) {
			return false;
		}
		if (!(sender instanceof Player)) {
			sender.sendMessage(FE.getMessages().onlyPlayers());
			return true;
		}
		Player player = (Player) sender;
		
		// enchantment check
		FordEnchant en = FE.getEnchant(args[0]);
		if (en == null) {
			sender.sendMessage(FE.getMessages().enchantNotFound(args[0]));
			return true;
		}
		
		// check level
		int level = 1;
		if (args.length > 1) {
			try {
				level = Integer.parseInt(args[1]);
			} catch (NumberFormatException e) {	}
		}
		
		boolean levelOverRide = sender.hasPermission("fordenchants.level-bypass");
		
		if (!levelOverRide && (level > en.getMaxLevel() || level < 0)) { 
			sender.sendMessage(FE.getMessages().unsupportedLevel(level, en.getMaxLevel()));
			return true;
		}
		
		// check item
		ItemStack item = player.getInventory().getItemInMainHand();
		if (item == null || item.getType() == Material.AIR) {
			sender.sendMessage(FE.getMessages().needItemInHand());
			return true;
		}
		
		// add enchantment
		try {
			ItemUtils.addEnchant(item, en, level, levelOverRide);
		} catch (IllegalArgumentException e) {
			sender.sendMessage(FE.getMessages().enchantmentNotApplicable(item, en));
			return true;
		}
		
		if (level != 0) {
			sender.sendMessage(FE.getMessages().addedEnchantment(en, level));
		} else {
			sender.sendMessage(FE.getMessages().removedEnchantment(en));
		}
		return true;
	}

}
