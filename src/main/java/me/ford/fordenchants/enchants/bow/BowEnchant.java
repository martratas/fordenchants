package me.ford.fordenchants.enchants.bow;

import org.bukkit.enchantments.EnchantmentTarget;
import org.bukkit.inventory.ItemStack;

import me.ford.fordenchants.FordEnchants;
import me.ford.fordenchants.enchants.FordEnchant;
import me.ford.fordenchants.utils.ItemUtils;

public abstract class BowEnchant extends FordEnchant {

	public BowEnchant(FordEnchants plugin, String name) {
		super(plugin, name);
	}

	@Override
	public boolean canEnchantItem(ItemStack item) {
		return ItemUtils.isBow(item);
	}

	@Override
	public EnchantmentTarget getItemTarget() {
		return EnchantmentTarget.BOW;
	}
}
