package me.ford.fordenchants.enchants.cooldowns;

public interface CooldownEnchant {
	
	public CooldownHandler getCooldownHandler(); 

}
