package me.ford.fordenchants.enchants.cooldowns;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.bukkit.entity.Entity;

public class CooldownHandler {
	private final Map<UUID, Long> cooldowns = new HashMap<>();
	private final long defaultCooldown;
	
	public CooldownHandler(long cooldown) {
		defaultCooldown = cooldown;
	}
	
	public void startCooldown(Entity entity) {
		startCooldown(entity, defaultCooldown);
	}
	
	public void startCooldown(Entity entity, long seconds) {
		cooldowns.put(entity.getUniqueId(), System.currentTimeMillis() + seconds * 1000L);
	}
	
	public boolean endCooldown(Entity entity) {
		return cooldowns.remove(entity.getUniqueId()) != null;
	}
	
	public boolean isOnCooldown(Entity entity) {
		Long end = cooldowns.get(entity.getUniqueId());
		if (end == null) return false;
		if (end < System.currentTimeMillis()) {
			cooldowns.remove(entity.getUniqueId());
			return false;
		}
		return true;
	}
	
	public long cooldownEnd(Entity entity) {
		Long end = cooldowns.get(entity.getUniqueId());
		if (end == null) return System.currentTimeMillis();
		return end;
	}
	
	public Map<UUID, Long> getCooldowns() {
		Map<UUID, Long> map = new HashMap<>(cooldowns); // clone
		return map;
	}

}
