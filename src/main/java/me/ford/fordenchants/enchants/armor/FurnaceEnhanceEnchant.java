package me.ford.fordenchants.enchants.armor;

import org.bukkit.ChatColor;
import org.bukkit.block.Furnace;
import org.bukkit.block.Smoker;
import org.bukkit.block.BlastFurnace;
import org.bukkit.block.Block;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.event.Event;
import org.bukkit.event.inventory.FurnaceSmeltEvent;

import me.ford.fordenchants.FordEnchants;
import me.ford.fordenchants.enchants.level.ArmorTotalLevel;
import me.ford.fordenchants.enchants.level.LevelDependence.EnforceZeroLevel;
import me.ford.fordenchants.enchants.responses.FurnaceSmeltResponse;
import me.ford.fordenchants.settings.enchants.EnchantSettings;
import me.ford.fordenchants.settings.enchants.armor.FurnaceEnhanceSettings;
import me.ford.fordenchants.utils.ItemUtils;

public class FurnaceEnhanceEnchant extends ArmorEnchant implements FurnaceSmeltResponse, ArmorTotalLevel, EnforceZeroLevel {	
	private static final String NAME = "furnace_enhance";
	private static final String DPNAME = ChatColor.GRAY + "Furnace Enhance";
	private static final int FURNACE_DEFAULT = 200;
	private static final int SPECIFIC_FURNACE_DEFAULT = 100;
	private final FurnaceEnhanceSettings settings;
	private final boolean specialExists;

	public FurnaceEnhanceEnchant(FordEnchants plugin) {
		super(plugin, NAME);
		settings = new FurnaceEnhanceSettings(FE);
		boolean exists = false;
		try {
			Class.forName("org.bukkit.block.BlastFurnace");
			exists = true;
		} catch (ClassNotFoundException e) {
		} finally {
			specialExists = exists;
		}
	}

	@Override
	public String getDisplayName() {
		return DPNAME;
	}

	@Override
	public String getName() {
		return NAME.toUpperCase();
	}

	@Override
	public int getMaxLevel() {
		return settings.maxLevel();
	}

	@Override
	public boolean conflictsWith(Enchantment other) {
		return false;
	}
	
	// RESPONSES

	@Override
	public ResponseType[] getResponseTypes() {
		return new ResponseType[] {ResponseType.FURNACE_SMELT};
	}

	@Override
	public int getLevel(Event event) {
		if (!(event instanceof FurnaceSmeltEvent)) {
			throw new IllegalArgumentException("Expected FurnaceSmeltEvent, got: " + event);
		}
		return ItemUtils.getTotalArmorLevelInArea(((FurnaceSmeltEvent) event).getBlock().getLocation(), settings.distance(), this);
	}
	
	// EFFECTS

	@Override
	public void onFurnaceSmelt(FurnaceSmeltEvent event, int level) {
		Furnace furnace = (Furnace) event.getBlock().getState();
		int def = FURNACE_DEFAULT;
		if (specialExists && (furnace instanceof BlastFurnace || furnace instanceof Smoker)) {
			def = SPECIFIC_FURNACE_DEFAULT;
		}
		int newTime = level == 0 ? def : (int) Math.floor(def / (settings.defaultSpeedup() + level * settings.speedupPerLevel()));
		
		if (level != 0 && settings.getDurability(level) > 0) ItemUtils.handleDurabilityInArea(event.getBlock().getLocation(), level, settings.distance(), this, 0, 1, Integer.MAX_VALUE);
		
		boolean needToChange = (level == 0 && furnace.getCookTimeTotal() != def) || (level > 0);
		if (needToChange) FE.getServer().getScheduler().runTask(FE, () -> update(event.getBlock(), newTime));		
	}
	
	private void update(Block block, int newTime) {
		Furnace furnace;
		try {
			furnace = (Furnace) block.getState();
		} catch (ClassCastException e) {
			FE.getLogger().warning("Furnace removed while trying to enhance it!");
			return;
		}
		furnace.setCookTimeTotal(newTime);
		furnace.update();
	}
	
	// settings

	@Override
	public EnchantSettings getSettings() {
		return settings;
	}

}
