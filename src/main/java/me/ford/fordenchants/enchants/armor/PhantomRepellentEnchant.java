package me.ford.fordenchants.enchants.armor;

import org.bukkit.ChatColor;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.enchantments.EnchantmentTarget;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Phantom;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityTargetLivingEntityEvent;
import org.bukkit.inventory.EntityEquipment;
import org.bukkit.inventory.ItemStack;

import me.ford.fordenchants.FordEnchants;
import me.ford.fordenchants.enchants.level.LevelDependence.IgnoreZeroLevel;
import me.ford.fordenchants.enchants.responses.EntityDamageByEntityResponse;
import me.ford.fordenchants.enchants.responses.EntityTargetLivingEntityResponse;
import me.ford.fordenchants.settings.enchants.EnchantSettings;
import me.ford.fordenchants.settings.enchants.armor.PhantomRepellentSettings;
import me.ford.fordenchants.utils.ItemUtils;
import me.ford.fordenchants.utils.NoEquipmentException;

public class PhantomRepellentEnchant extends ArmorEnchant implements EntityTargetLivingEntityResponse, EntityDamageByEntityResponse, IgnoreZeroLevel {
	private static final String NAME = "phantom_repellent";
	private static final String DPNAME = ChatColor.GRAY + "Phantom Repellent";
	private final PhantomRepellentSettings settings;

	public PhantomRepellentEnchant(FordEnchants plugin) {
		super(plugin, NAME);
		settings = new PhantomRepellentSettings(FE);
	}
	
	@Override
	public String getDisplayName() {
		return DPNAME;
	}

	@Override
	public boolean canEnchantItem(ItemStack stack) {
		return ItemUtils.isHelmet(stack);
	}

	@Override
	public boolean conflictsWith(Enchantment arg0) {
		return false;
	}

	@Override
	public EnchantmentTarget getItemTarget() {
		return EnchantmentTarget.ARMOR_HEAD;
	}

	@Override
	public int getMaxLevel() {
		return 1;
	}

	@Override
	public String getName() {
		return NAME.toUpperCase();
	}
	
	// RESPONSE

	@Override
	public ResponseType[] getResponseTypes() {
		return new ResponseType[] {ResponseType.ENTITY_DAMAGE_BY_ENTITY, ResponseType.ENTITY_TARGET_LIVING_ENTITY};
	}

	@Override
	public int getLevel(Event event) {
		if (event instanceof EntityDamageByEntityEvent) {
			return getLevel(((EntityDamageByEntityEvent) event).getDamager());
		} else if (event instanceof EntityTargetLivingEntityEvent) {
			return getLevel(((EntityTargetLivingEntityEvent) event).getTarget());
		} else {
			throw new IllegalArgumentException("Was expecting EntityTargetLivingEntityEvent or EntityDamageByEntityEvent, got:" + event);
		}
	}

	@Override
	public int getLevel(Entity entity) {
		return ItemUtils.getHelmetLevel(entity, this);
	}
	
	// EFFECT

	@Override
	public void onEvent(Event event, int level) {
		if (event instanceof EntityDamageByEntityEvent) {
			EntityDamageByEntityResponse.super.onEvent(event, level);
		} else {
			EntityTargetLivingEntityResponse.super.onEvent(event, level);
		}
	}

	@Override
	public void onEntityTargetLivingEntity(EntityTargetLivingEntityEvent event, int level) {
		if (!(event.getEntity() instanceof Phantom)) {
			return;
		}
		if (settings.getDurability(level) > 0) handleDurability(event.getTarget(), settings.getDurability(level));
		event.setCancelled(true);
	}
	
	private void handleDurability(LivingEntity entity, int nr) {
		EntityEquipment eq = entity.getEquipment();
		if (eq == null) throw new NoEquipmentException(entity);
		ItemStack helm = eq.getHelmet();
		ItemUtils.reduceDurability(helm, nr);
		if (ItemUtils.isBroken(helm)) {
			helm = null;
		}
		eq.setHelmet(helm);
	}
	
	@Override
	public void onEntityDamageByEntity(EntityDamageByEntityEvent event, int level) {
		if (!settings.stopDamage()) {
			return;
		}
		if (!(event.getDamager() instanceof Phantom)) {
			return;
		}
		event.setCancelled(true);
	}
	
	// settings

	@Override
	public EnchantSettings getSettings() {
		return settings;
	}

}
