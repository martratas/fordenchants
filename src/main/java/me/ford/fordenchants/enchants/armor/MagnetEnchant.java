package me.ford.fordenchants.enchants.armor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.block.Container;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.Event;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.projectiles.ProjectileSource;

import me.ford.fordenchants.FordEnchants;
import me.ford.fordenchants.enchants.cooldowns.CooldownHandler;
import me.ford.fordenchants.enchants.level.LevelDependence.IgnoreZeroLevel;
import me.ford.fordenchants.enchants.responses.BlockBreakResponse;
import me.ford.fordenchants.enchants.responses.EntityDeathResponse;
import me.ford.fordenchants.settings.enchants.EnchantSettings;
import me.ford.fordenchants.settings.enchants.armor.MagnetSettings;
import me.ford.fordenchants.utils.ItemUtils;

public class MagnetEnchant extends ArmorEnchant implements BlockBreakResponse, EntityDeathResponse, IgnoreZeroLevel {
	private static final String NAME = "magnet";
	private static final String DPNAME = ChatColor.GRAY + "Magnet";
	private final MagnetSettings settings;
	private final CooldownHandler cooldowns;

	public MagnetEnchant(FordEnchants plugin) {
		super(plugin, NAME);
		settings = new MagnetSettings(FE);
		cooldowns = new CooldownHandler(2); // 2 seconds
	}

	@Override
	public String getDisplayName() {
		return DPNAME;
	}

	@Override
	public String getName() {
		return NAME.toUpperCase();
	}

	@Override
	public int getMaxLevel() {
		return settings.maxLevel();
	}

	@Override
	public boolean conflictsWith(Enchantment ench) {
		return false;
	}
	
	// RESPONSE
	
	@Override
	public ResponseType[] getResponseTypes() {
		return new ResponseType[] {ResponseType.BLOCK_BREAK};
	}

	@Override
	public int getLevel(Event event) {
		if (event instanceof BlockBreakEvent) {
			return getLevel(((BlockBreakEvent) event).getPlayer());
		} else if (event instanceof EntityDeathEvent) {
			Entity damager = getDamager((EntityDeathEvent) event);
			if (damager == null) return 0;
			return getLevel(damager);
		}
		throw new IllegalArgumentException("Expected BlockBreakEvent or EntityDeathEvent, got " + event);
	}
	
	private Entity getDamager(EntityDeathEvent event) {
		if (!(event.getEntity().getLastDamageCause() instanceof EntityDamageByEntityEvent)) {
			return null;
		}
		EntityDamageByEntityEvent eEvent = (EntityDamageByEntityEvent) event.getEntity().getLastDamageCause();
		Entity damager = eEvent.getDamager();
		if (damager instanceof Projectile) {
			ProjectileSource source = ((Projectile) damager).getShooter();
			if (source instanceof Entity) {
				damager = (Entity) source;
			}
		}
		return damager;
	}

	@Override
	public int getLevel(Entity entity) {
		return ItemUtils.getTotalArmorLevelOf(entity, this);
	}

	@Override
	public void onEvent(Event event, int level) {
		if (event instanceof BlockBreakEvent) {
			BlockBreakResponse.super.onEvent(event, level);
		} else if (event instanceof EntityDeathEvent) {
			EntityDeathResponse.super.onEvent(event, level);
		} else {
			throw new IllegalArgumentException("Expected BlockBreakEvent or EntityDeathEvent, got" + event);
		}
	}
	
	// EFFECTS

	@Override
	public void onBlockBreak(BlockBreakEvent event, int level) {
		Player player = event.getPlayer();
		ItemStack tool = player.getInventory().getItemInMainHand();
		Block block = event.getBlock();
		Collection<ItemStack> drops = block.getDrops(tool);
		magnet(drops, player, block.getLocation(), level, false);
		
		// for containers
		BlockState state = block.getState();
		if (state instanceof Container && settings.getFromInventory()) {
			Inventory inv = ((Container) state).getInventory();
			List<ItemStack> contents = new ArrayList<>();
			for (ItemStack item : inv.getContents()) {
				if (item != null && item.getType() != Material.AIR) {
					contents.add(item);
				}
			}
			inv.clear();
			magnet(contents, player, block.getLocation(), 0, false);
		}
		event.setDropItems(false);
	}

	@Override // KILLS
	public void onEntityDeath(EntityDeathEvent event, int level) {
		if (!settings.getFromKills()) return;
		
		Entity damager = getDamager(event);
		if (!(damager instanceof Player)) {
			return;
		}
		Player player = (Player) damager;
		
		List<ItemStack> contents = event.getDrops();
		
		magnet(contents, player, event.getEntity().getLocation(), level, true);
	}
	
	private boolean magnet(Collection<ItemStack> drops, Player player, Location loc, int level, boolean dropInCollection) {
		Map<Integer, ItemStack> leftOvers = player.getInventory().addItem(drops.toArray(new ItemStack[0]));
		if (settings.dropLeftovers() && !leftOvers.isEmpty() && !dropInCollection) {
			for (ItemStack item : leftOvers.values()) {
				loc.getWorld().dropItemNaturally(loc, item);
			}
			if (!cooldowns.isOnCooldown(player) && settings.sendFullMessage()) {
				player.sendMessage(settings.getFullMessage(FE.getMessages()));
				cooldowns.startCooldown(player);
			} 
		} else if (dropInCollection) { // in case of death event need to only have leftovers in collection
			drops.clear();
			drops.addAll(leftOvers.values());
		}
		double chance = settings.chanceToLoseDurability(level);
		double nr = FE.getRandomProvider().nextDouble();
		if (chance > nr) {
			ItemUtils.handleDurabilityOfEntity(player, ItemUtils.getArmorLevel(player, this), this, 1, 0, level);
		}
		return !leftOvers.isEmpty();
	}
	
	// settings

	@Override
	public EnchantSettings getSettings() {
		return settings;
	}

}
