package me.ford.fordenchants.enchants.armor;

import org.bukkit.enchantments.EnchantmentTarget;
import org.bukkit.inventory.ItemStack;

import me.ford.fordenchants.FordEnchants;
import me.ford.fordenchants.enchants.FordEnchant;
import me.ford.fordenchants.utils.ItemUtils;

public abstract class ArmorEnchant extends FordEnchant {

	public ArmorEnchant(FordEnchants plugin, String name) {
		super(plugin, name);
	}

	@Override
	public boolean canEnchantItem(ItemStack item) {
		return ItemUtils.isArmor(item);
	}

	@Override
	public EnchantmentTarget getItemTarget() {
		return EnchantmentTarget.ARMOR;
	}

}
