package me.ford.fordenchants.enchants.armor;

import org.bukkit.ChatColor;
import org.bukkit.block.CreatureSpawner;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.event.Event;
import org.bukkit.event.entity.SpawnerSpawnEvent;

import me.ford.fordenchants.FordEnchants;
import me.ford.fordenchants.enchants.level.ArmorTotalLevel;
import me.ford.fordenchants.enchants.level.LevelDependence.EnforceZeroLevel;
import me.ford.fordenchants.enchants.responses.SpawnerSpawnResponse;
import me.ford.fordenchants.settings.enchants.EnchantSettings;
import me.ford.fordenchants.settings.enchants.armor.SpawnerEnhanceSettings;
import me.ford.fordenchants.utils.ItemUtils;

public class SpawnerEnhanceEnchant extends ArmorEnchant implements SpawnerSpawnResponse, ArmorTotalLevel, EnforceZeroLevel {
	private static final String NAME = "spawner_enhance";
	private static final String DPNAME = ChatColor.GRAY + "Spawner Enhance";
	private static final int SPAWNER_MAX_ENTITY = 6; // DEFAULT
	private static final int SPAWNER_MIN_DELAY = 200; // DEFAULT
	private static final int SPAWNER_MAX_DELAY = 800; // DEFAULT
	private static final int SPAWNER_DISTANCE = 16;
	private final SpawnerEnhanceSettings settings;
	
	public SpawnerEnhanceEnchant(FordEnchants plugin) {
		super(plugin, NAME);
		settings = new SpawnerEnhanceSettings(FE);
	}

	@Override
	public String getDisplayName() {
		return DPNAME;
	}

	@Override
	public boolean conflictsWith(Enchantment enchant) {
		return false;
	}

	@Override
	public int getMaxLevel() {
		return settings.maxLevel(); // default 5 (config)
	}

	@Override
	public String getName() {
		return NAME.toUpperCase();
	}
	
	// RESPONSE

	@Override
	public ResponseType[] getResponseTypes() {
		return new ResponseType[] {ResponseType.SPAWNER_SPAWN};
	}

	@Override
	public int getLevel(Event event) {
		if (event instanceof SpawnerSpawnEvent) {
			return ItemUtils.getTotalArmorLevelInArea(((SpawnerSpawnEvent) event).getLocation(), SPAWNER_DISTANCE, this);
		} else {
			throw new IllegalArgumentException("Was expecting SpawnerSpawnEvent, got " + event);
		}
	}
	
	// EFFECTS
	
	@Override
	public void onSpawnerSpawn(SpawnerSpawnEvent event, int levels) {
		CreatureSpawner spawner = event.getSpawner();
		if (levels == 0) {
			reset(spawner);
			return;
		}
		boost(spawner, levels);
		if (levels != 0 && settings.getDurability(levels) > 0) ItemUtils.handleDurabilityInArea(event.getLocation(), levels, SPAWNER_DISTANCE, this, 0, 1, Integer.MAX_VALUE);
	}
	
	private void boost(CreatureSpawner spawner, int levels) {
		spawner.setMaxNearbyEntities(SPAWNER_MAX_ENTITY + levels * settings.getNearbyMultiplier());
		int minDelay = SPAWNER_MIN_DELAY - levels * settings.getMinDelayMultiplier();
		int maxDelay = SPAWNER_MAX_DELAY - levels * settings.getMaxDelayMultiplier();
		if (minDelay < 0) minDelay = 0;
		if (maxDelay < 0) maxDelay = 1;
		if (maxDelay <= minDelay) maxDelay = minDelay + 1;
		if (spawner.getMaxSpawnDelay() < minDelay) {
			spawner.setMaxSpawnDelay(maxDelay); // in case I'm "boosting" to a much smaller value
		}
		spawner.setMinSpawnDelay(minDelay);
		spawner.setMaxSpawnDelay(maxDelay);
		spawner.update();
	}
	
	private void reset(CreatureSpawner spawner) {
		boolean changed = false;
		if (spawner.getMaxNearbyEntities() != SPAWNER_MAX_ENTITY) {
			spawner.setMaxNearbyEntities(SPAWNER_MAX_ENTITY);
			changed = true;
		}
		if (spawner.getMaxSpawnDelay() != SPAWNER_MAX_DELAY) {
			spawner.setMaxSpawnDelay(SPAWNER_MAX_DELAY);
			changed = true;
		}
		if (spawner.getMinSpawnDelay() != SPAWNER_MIN_DELAY) {
			spawner.setMinSpawnDelay(SPAWNER_MIN_DELAY);
			changed = true;
		}
		if (changed) spawner.update(); // only update if necessary
	}
	
	// settings

	@Override
	public EnchantSettings getSettings() {
		return settings;
	}

}
