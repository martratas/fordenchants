package me.ford.fordenchants.enchants.tool;

import org.bukkit.enchantments.EnchantmentTarget;
import org.bukkit.inventory.ItemStack;

import me.ford.fordenchants.FordEnchants;
import me.ford.fordenchants.enchants.FordEnchant;
import me.ford.fordenchants.utils.ItemUtils;

/**
 * ToolEnchant
 */
public abstract class ToolEnchant extends FordEnchant {

    public ToolEnchant(FordEnchants plugin, String name) {
        super(plugin, name);
    }

    @Override
    public EnchantmentTarget getItemTarget() {
        return EnchantmentTarget.TOOL;
    }

	@Override
	public boolean canEnchantItem(ItemStack item) {
		return ItemUtils.isTool(item);
	}

}