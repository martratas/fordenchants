package me.ford.fordenchants.enchants.tool;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.player.PlayerMoveEvent;

import me.ford.fordenchants.FordEnchants;
import me.ford.fordenchants.enchants.level.LevelDependence.EnforceZeroLevel;
import me.ford.fordenchants.enchants.responses.PlayerMoveBlockResponse;
import me.ford.fordenchants.settings.enchants.EnchantSettings;
import me.ford.fordenchants.settings.enchants.tool.XRayEnchantSettings;
import me.ford.fordenchants.utils.ItemUtils;

/**
 * XRayEnchant
 */
public class XRayEnchant extends ToolEnchant
        implements PlayerMoveBlockResponse/*, PlayerChangedMainHandResponse*/, EnforceZeroLevel {
    private static final String NAME = "xray";
    private static final String DPNAME = ChatColor.GRAY + "X-Ray";
    private final XRayEnchantSettings settings;
    private final Map<UUID, HiddenBlocks> hiddenBlocks = new HashMap<>();

    public XRayEnchant(FordEnchants plugin) {
        super(plugin, NAME);
        settings = new XRayEnchantSettings(plugin);
    }

    @Override
    public String getName() {
        return NAME.toUpperCase();
    }

    @Override
    public String getDisplayName() {
        return DPNAME;
    }

    @Override
    public EnchantSettings getSettings() {
        return settings;
    }

    @Override
    public int getMaxLevel() {
        return settings.maxLevel();
    }

    @Override
    public boolean conflictsWith(Enchantment other) {
        return false;
    }

    @Override
    public void onPlayerMove(PlayerMoveEvent event, int level) { // since it's PlayerMoveBlockRespone, they've moved an
                                                                 // entire block
        Player player = event.getPlayer();
        UUID id = player.getUniqueId();
        if (level == 0 && !hiddenBlocks.containsKey(id))
            return;
        HiddenBlocks hidden = hiddenBlocks.get(id);
        if (level == 0 && hidden != null) {
            sendNormalBlocks(player, hidden);
            hiddenBlocks.remove(id);
            return;
        }
        if (hidden == null) {
            hidden = new HiddenBlocks();
            hiddenBlocks.put(id, hidden);
        }
        sendXRayBlocks(player, hidden, level);
    }

    private void sendXRayBlocks(Player player, HiddenBlocks hidden, int level) {
        int dist = settings.defaultDistance() + level * settings.distancePerLevel();
        BlocksOnChange onChange = hidden.newBlocksOnMoveTo(player.getLocation().getBlock(), dist);
        for (Block block : onChange.getBlocksToRemove()) {
            player.sendBlockChange(block.getLocation(), block.getBlockData());
        }
        for (Block block : onChange.getNewBlocks()) { // TODO - list of ores?
            if (block.getType().isSolid() && !block.getType().name().endsWith("_ORE")) {
                player.sendBlockChange(block.getLocation(), Material.BARRIER.createBlockData());
            }
        }
    }

    private void sendNormalBlocks(Player player, HiddenBlocks hidden) {
        for (Block block : hidden.getHiddenBlocks()) {
            player.sendBlockChange(block.getLocation(), block.getBlockData());
        }
    }

    @Override
    public ResponseType[] getResponseTypes() {
        return new ResponseType[] { ResponseType.PLAYER_MOVE };
    }

    @Override
    public int getLevel(Event event) {
        if (!(event instanceof PlayerMoveEvent)) {
            throw new IllegalArgumentException("Expected PlayerMoveEvent, got: " + event);
        }
        PlayerMoveEvent pmEvent = (PlayerMoveEvent) event;
        return ItemUtils.getLevel(pmEvent.getPlayer().getInventory().getItemInMainHand(), this);
    }

    @Override
    public int getLevel(Entity entity) {
        return ItemUtils.getMainHandLevel(entity, this);
    }

    private class HiddenBlocks {
        private final Set<Block> hiddenBlocks = new HashSet<>();

        private BlocksOnChange newBlocksOnMoveTo(Block newCenter, int dist) {
            Set<Block> newBlocks = new HashSet<>();
            Set<Block> blocksToRemove = getHiddenBlocks();
            for (int x = -dist; x <= dist; x++) {
                for (int y = -dist; y <= dist; y++) {
                    for (int z = -dist; z <= dist; z++) {
                        Block block = newCenter.getLocation().add(x, y, z).getBlock();
                        if (!hiddenBlocks.contains(block)) {
                            newBlocks.add(block);
                        } else {
                            blocksToRemove.remove(block);
                        }
                    }
                }
            }
            hiddenBlocks.removeAll(blocksToRemove);
            hiddenBlocks.addAll(newBlocks);
            return new BlocksOnChange(blocksToRemove, newBlocks);
        }

        private Set<Block> getHiddenBlocks() {
            return new HashSet<>(hiddenBlocks);
        }

    }

    private class BlocksOnChange {
        private final Set<Block> blocksToRemove;
        private final Set<Block> newBlocks;

        private BlocksOnChange(Set<Block> blocksToRemove, Set<Block> newBlocks) {
            this.blocksToRemove = blocksToRemove;
            this.newBlocks = newBlocks;
        }

        private Set<Block> getBlocksToRemove() {
            return blocksToRemove;
        }

        private Set<Block> getNewBlocks() {
            return newBlocks;
        }

    }

}