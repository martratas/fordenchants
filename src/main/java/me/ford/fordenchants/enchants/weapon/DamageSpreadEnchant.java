package me.ford.fordenchants.enchants.weapon;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.AnimalTamer;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Tameable;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

import me.ford.fordenchants.FordEnchants;
import me.ford.fordenchants.enchants.level.LevelDependence.IgnoreZeroLevel;
import me.ford.fordenchants.enchants.responses.EntityDamageByEntityResponse;
import me.ford.fordenchants.settings.enchants.EnchantSettings;
import me.ford.fordenchants.settings.enchants.weapon.DamageSpreadSettings;
import me.ford.fordenchants.utils.ItemUtils;
import me.ford.fordenchants.utils.MyFixedSizeEntityList;
import me.ford.fordenchants.utils.Utils;

public class DamageSpreadEnchant extends WeaponEnchant implements EntityDamageByEntityResponse, IgnoreZeroLevel {
	private static final String NAME = "damage_spread";
	private static final String DPNAME = ChatColor.GRAY + "Damage Spread";
	private static final Set<EntityType> immuneEntities = EnumSet.of(EntityType.ARMOR_STAND, EntityType.ITEM_FRAME, EntityType.LEASH_HITCH, EntityType.PAINTING,
			EntityType.MINECART, EntityType.MINECART_CHEST, EntityType.MINECART_COMMAND, EntityType.MINECART_FURNACE, EntityType.MINECART_HOPPER, EntityType.MINECART_MOB_SPAWNER, EntityType.MINECART_TNT);
	
	private final DamageSpreadSettings settings;
	
	public DamageSpreadEnchant(FordEnchants plugin) {
		super(plugin, NAME);
		settings = new DamageSpreadSettings(FE);
	}

	@Override
	public String getDisplayName() {
		return DPNAME;
	}

	@Override
	public String getName() {
		return NAME.toUpperCase();
	}

	@Override
	public int getMaxLevel() {
		return settings.maxLevel();
	}

	@Override
	public int getStartLevel() {
		return 0;
	}

	@Override
	public boolean isTreasure() {
		return false;
	}

	@Override
	public boolean isCursed() {
		return false;
	}

	@Override
	public boolean conflictsWith(Enchantment other) {
		return false;
	}
	
	// RESPONSE

	@Override
	public int getLevel(Event event) {
		if (event instanceof EntityDamageByEntityEvent) {
			return getLevel(((EntityDamageByEntityEvent) event).getDamager());
		} else {
			throw new IllegalArgumentException("Expected EntityDamageByEntityEvent, got: " + event);
		}
	}

	@Override
	public int getLevel(Entity entity) {
		return ItemUtils.getMainHandLevel(entity, this);
	}

	@Override
	public ResponseType[] getResponseTypes() {
		return new ResponseType[] {ResponseType.ENTITY_DAMAGE_BY_ENTITY};
	}
	
	// EFFECT
	private final Set<UUID> spreading = new HashSet<>();
	
	@Override
	public void onEntityDamageByEntity(EntityDamageByEntityEvent event, int level) {
		if (immuneEntities.contains(event.getEntityType())) {
			return;
		}
		// chance
		double chance = settings.defaultChance() + level * settings.chancePerLevel();
		double nr = FE.getRandomProvider().nextDouble();
		if (chance <= nr) {
			return;
		}
		
		Entity damager = event.getDamager();
		// tests
		if (!settings.affectPlayers() && event.getEntity() instanceof Player && !Utils.isNPC(event.getEntity())) {
			return; // in case of NPCs, deal damage
		}
		UUID uuid = damager.getUniqueId();
		if (!spreading.contains(uuid)) {
			// spread
			spreading.add(uuid);
			int connections = settings.connectionsDefault() + level * settings.connectionsPerLevel();
			FE.getServer().getScheduler().runTask(FE, () -> {
				int nrOfConnections = spread(damager, event.getEntity(), event.getFinalDamage(), connections);
				spreading.remove(uuid);
				if (nrOfConnections > 0 && nrOfConnections * settings.getDurability(level) > 0) ItemUtils.handleMainHandDurability(damager, nrOfConnections * settings.getDurability(level));
			});
			if (settings.sendMessage()) {
				damager.sendMessage(settings.getMessage(FE.getMessages(), connections));
			}
			
		}
	}
	
	private int spread(Entity damager, Entity entity, double initialDamage, int connections) {
		MyFixedSizeEntityList targets = new MyFixedSizeEntityList(connections);
		List<Double> distances = new ArrayList<>(); // all distances - just counting number of them that are smaller
		double dist = settings.connectionDistance();
		
		for (Entity e : entity.getNearbyEntities(dist, dist, dist)) {
			if (e.equals(entity)) {
				continue; // that entity already gets damaged
			}
			if (!(e instanceof LivingEntity)) {
				continue;
			}
			LivingEntity le = (LivingEntity) e;
			if (immuneEntities.contains(e.getType())) {
				continue;
			}
			if (!settings.affectPlayers() && e instanceof Player && Utils.isNPC(e)) {
				continue;
			}
			if (!settings.affectSelf() && e.getUniqueId().equals(damager.getUniqueId())) {
				continue;
			}
			if (!settings.attackOwned() && e instanceof Tameable) {
				AnimalTamer owner = ((Tameable) e).getOwner();
				if (owner != null && owner.getUniqueId().equals(damager.getUniqueId())) {
					continue;
				}
			}
			double cdist2 = entity.getLocation().distanceSquared(e.getLocation());
			distances.add(cdist2);
			long count = distances.stream().filter((d) -> d < cdist2).count(); // how many are smaller
			targets.putAndPush(Math.toIntExact(count), le);
		}
		if (targets.isEmpty()) {
			return 0;
		}
		damageTargets(targets, initialDamage * settings.connectionReduction(), damager);
		if (settings.showParticles()) {
			particlesToTargets(targets);
			FE.getServer().getScheduler().runTaskLater(FE, () -> particlesToTargets(targets), 2L);
		}
		return targets.size();
	}
	
	private void damageTargets(List<LivingEntity> targets, double damage, Entity damager) {
		for (final LivingEntity e : targets) {
			if (e == null) continue;
			e.damage(damage, damager);
		}
	}
	
	private void particlesToTargets(List<LivingEntity> targets) {
		int nrOfParticles = settings.nrOfParticles();
		for (LivingEntity e : targets) {
			if (e == null) continue;
			Location loc = e.getLocation();
			loc.getWorld().spawnParticle(Particle.FLAME, loc, nrOfParticles);
		}
	}
	
	// settings

	@Override
	public EnchantSettings getSettings() {
		return settings;
	}

}
