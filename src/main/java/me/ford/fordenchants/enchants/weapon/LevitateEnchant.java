package me.ford.fordenchants.enchants.weapon;

import java.util.EnumSet;
import java.util.Set;

import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

import me.ford.fordenchants.FordEnchants;
import me.ford.fordenchants.enchants.cooldowns.CooldownEnchant;
import me.ford.fordenchants.enchants.cooldowns.CooldownHandler;
import me.ford.fordenchants.enchants.level.LevelDependence.IgnoreZeroLevel;
import me.ford.fordenchants.enchants.responses.PlayerInteractResponse;
import me.ford.fordenchants.settings.enchants.EnchantSettings;
import me.ford.fordenchants.settings.enchants.weapon.LevitateSettings;
import me.ford.fordenchants.utils.ItemUtils;
import me.ford.fordenchants.utils.Utils;
import net.md_5.bungee.api.ChatColor;

public class LevitateEnchant extends WeaponEnchant implements PlayerInteractResponse, IgnoreZeroLevel, CooldownEnchant {
	private static final String NAME = "levitate";
	private static final String DPNAME = ChatColor.GRAY + "Levitate";
	private static final double SKIP = 1.675;
	private static final Set<Action> RIGHT_CLICK = EnumSet.of(Action.RIGHT_CLICK_AIR);
	private final LevitateSettings settings;
	private final CooldownHandler cooldownHandler;

	public LevitateEnchant(FordEnchants plugin) {
		super(plugin, NAME);
		settings = new LevitateSettings(FE);
		cooldownHandler = new CooldownHandler(settings.cooldown());
	}

	@Override
	public String getName() {
		return NAME.toUpperCase();
	}

	@Override
	public String getDisplayName() {
		return DPNAME;
	}

	@Override
	public boolean conflictsWith(Enchantment arg0) {
		return false;
	}

	@Override
	public int getMaxLevel() {
		return settings.maxLevel();
	}
	
	// RESPONSE

	@Override
	public ResponseType[] getResponseTypes() {
		return new ResponseType[] {ResponseType.PLAYER_INTERACT};
	}

	@Override
	public int getLevel(Event event) {
		if (event instanceof PlayerInteractEvent) {
			return getLevel(((PlayerInteractEvent) event).getPlayer());
		}
		throw new IllegalArgumentException("Expected PlayerInteractAtEntityEvent, got: " + event);
	}

	@Override
	public int getLevel(Entity entity) {
		return ItemUtils.getMainHandLevel(entity, this);
	}
	
	// EFFECTS

	@Override
	public void onPlayerInteract(PlayerInteractEvent event, int level) {
		if (event.getHand() != EquipmentSlot.HAND) return;
		if (!RIGHT_CLICK.contains(event.getAction())) return;
		Player player = event.getPlayer();
		if (cooldownHandler.isOnCooldown(player)) {
			return;
		}
		// chance
		double chance = settings.defaultChance() + level * settings.chancePerLevel();
		double nr = FE.getRandomProvider().nextDouble();
		if (chance <= nr) {
			return;
		}
		
		Vector dir = player.getLocation().getDirection();
		
		LivingEntity target = (LivingEntity) Utils.rayTrace(player.getEyeLocation(), dir, settings.range(), SKIP, (e) -> (!settings.isExempt(e.getType()) && e instanceof LivingEntity));
		
		// players
		if (!settings.affectPlayers() && target instanceof Player && !Utils.isNPC(target)) {
			return; // NPCs are not players
		}
		
		if (target == null) return;
		double duration = levitate(target, level);
		if (settings.sendMessage()) {
			player.sendMessage(settings.getMessage(FE.getMessages(), level, duration));
		}
		if (level != 0 && settings.getDurability(level) > 0) ItemUtils.handleMainHandDurability(player, settings.getDurability(level));
	}
	
	private double levitate(LivingEntity target, int level) {
		double duration = (settings.defaultDuration() + level * settings.durationPerLevel());
		int durTicks = (int) (duration * 20);
		int effectLevel = settings.defaultLevel() + level * settings.levelPerLevel();
		target.addPotionEffect(new PotionEffect(PotionEffectType.LEVITATION, durTicks, effectLevel), true);
		return duration;
	}
	
	// COOLDOWN

	@Override
	public CooldownHandler getCooldownHandler() {
		return cooldownHandler;
	}
	
	// settings

	@Override
	public EnchantSettings getSettings() {
		return settings;
	}

}
