package me.ford.fordenchants.enchants.weapon;

import java.util.EnumSet;
import java.util.Set;

import org.bukkit.ChatColor;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.util.Vector;

import me.ford.fordenchants.FordEnchants;
import me.ford.fordenchants.enchants.cooldowns.CooldownEnchant;
import me.ford.fordenchants.enchants.cooldowns.CooldownHandler;
import me.ford.fordenchants.enchants.level.LevelDependence.IgnoreZeroLevel;
import me.ford.fordenchants.enchants.responses.PlayerInteractResponse;
import me.ford.fordenchants.settings.enchants.EnchantSettings;
import me.ford.fordenchants.settings.enchants.weapon.PullSettings;
import me.ford.fordenchants.utils.ItemUtils;
import me.ford.fordenchants.utils.Utils;

public class PullEnchant extends WeaponEnchant implements PlayerInteractResponse, IgnoreZeroLevel, CooldownEnchant {
	private static final String NAME = "pull";
	private static final String DPNAME = ChatColor.GRAY + "Pull";
	private static final double SKIP = 1.675;
	private static final Set<Action> RIGHT_CLICK = EnumSet.of(Action.RIGHT_CLICK_AIR);
	private final PullSettings settings;
	private final CooldownHandler cooldownHandler;

	public PullEnchant(FordEnchants plugin) {
		super(plugin, NAME);
		settings = new PullSettings(FE);
		cooldownHandler = new CooldownHandler(settings.cooldown());
	}

	@Override
	public String getDisplayName() {
		return DPNAME;
	}

	@Override
	public String getName() {
		return NAME.toUpperCase();
	}

	@Override
	public int getMaxLevel() {
		return settings.maxLevel();
	}

	@Override
	public boolean conflictsWith(Enchantment ench) {
		return false;
	}
	
	// RESPONSE

	@Override
	public ResponseType[] getResponseTypes() {
		return new ResponseType[] {ResponseType.PLAYER_INTERACT};
	}

	@Override
	public int getLevel(Event event) {
		if (event instanceof PlayerInteractEvent) {
			return getLevel(((PlayerInteractEvent) event).getPlayer());
		}
		throw new IllegalArgumentException("Expected PlayerInteractEvent, got: " + event);
	}

	@Override
	public int getLevel(Entity entity) {
		return ItemUtils.getMainHandLevel(entity, this);
	}
	
	// EFFECT

	@Override
	public void onPlayerInteract(PlayerInteractEvent event, int level) {
		if (event.getHand() != EquipmentSlot.HAND) return;
		if (!RIGHT_CLICK.contains(event.getAction())) return;
		Player player = event.getPlayer();
		
		// cooldown
		if (cooldownHandler.isOnCooldown(player)) {
			return;
		}
		
		// chance
		double chance = settings.defaultChance() + level * settings.chancePerLevel();
		double nr = FE.getRandomProvider().nextDouble();
		if (chance <= nr) {
			return;
		}
		
		Vector dir = player.getLocation().getDirection();
		
		Entity target = Utils.rayTrace(player.getEyeLocation(), dir, settings.range(), SKIP, (e) -> !settings.isExempt(e.getType()));
		if (target == null) return;
		
		// players
		if (!settings.affectPlayers() && target instanceof Player && !Utils.isNPC(target)) {
			return; // NPCs are not players
		}
		
		dir.normalize().multiply(-(settings.defaultStrength() + level * settings.strengthPerLevel()));
		target.setVelocity(dir);
		cooldownHandler.startCooldown(player);
		if (settings.sendMessage()) {
			player.sendMessage(settings.getMessage(FE.getMessages(), level));
		}
		if (level != 0 && settings.getDurability(level) > 0) ItemUtils.handleMainHandDurability(player, settings.getDurability(level));
	}
	
	// Cooldown

	@Override
	public CooldownHandler getCooldownHandler() {
		return cooldownHandler;
	}
	
	// settings

	@Override
	public EnchantSettings getSettings() {
		return settings;
	}

}
