package me.ford.fordenchants.enchants.weapon;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.attribute.Attribute;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.AnimalTamer;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Wolf;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.server.PluginDisableEvent;

import me.ford.fordenchants.FordEnchants;
import me.ford.fordenchants.enchants.cooldowns.CooldownEnchant;
import me.ford.fordenchants.enchants.cooldowns.CooldownHandler;
import me.ford.fordenchants.enchants.level.LevelDependence.IgnoreZeroLevel;
import me.ford.fordenchants.enchants.responses.EntityDamageByEntityResponse;
import me.ford.fordenchants.enchants.responses.PlayerJoinResponse;
import me.ford.fordenchants.enchants.responses.PlayerQuitResponse;
import me.ford.fordenchants.enchants.responses.PluginDisableResponse;
import me.ford.fordenchants.enchants.responses.Response;
import me.ford.fordenchants.enchants.responses.ZeroLevelResponse;
import me.ford.fordenchants.settings.enchants.EnchantSettings;
import me.ford.fordenchants.settings.enchants.weapon.SummonWolfSettings;
import me.ford.fordenchants.utils.ItemUtils;
import me.ford.fordenchants.utils.Utils;

public class SummonWolfEnchant extends WeaponEnchant implements EntityDamageByEntityResponse, PlayerJoinResponse, PlayerQuitResponse, PluginDisableResponse, ZeroLevelResponse, IgnoreZeroLevel, CooldownEnchant {
	private static final String NAME = "summon_wolf";
	private static final String DPNAME = ChatColor.GRAY + "Summon Wolf";
	private static final String METAKEY = "FordEnchants_" + NAME + "_CUSTOM"; 
	private static final Set<EntityType> immuneEntities = EnumSet.of(EntityType.ARMOR_STAND, EntityType.ITEM_FRAME, EntityType.LEASH_HITCH, EntityType.PAINTING,
														EntityType.MINECART, EntityType.MINECART_CHEST, EntityType.MINECART_COMMAND, EntityType.MINECART_FURNACE, EntityType.MINECART_HOPPER, EntityType.MINECART_MOB_SPAWNER, EntityType.MINECART_TNT);
	private final SummonWolfSettings settings;
	private final Map<UUID, Long> summoned = new HashMap<>();
	private final CooldownHandler cooldownHandler;

	public SummonWolfEnchant(FordEnchants plugin) {
		super(plugin, NAME);
		settings = new SummonWolfSettings(FE);
		cooldownHandler = new CooldownHandler(settings.wolfSpawnDelay());
	}

	@Override
	public String getDisplayName() {
		return DPNAME;
	}

	@Override
	public String getName() {
		return NAME.toUpperCase();
	}

	@Override
	public int getMaxLevel() {
		return settings.maxLevel();
	}

	@Override
	public boolean conflictsWith(Enchantment other) {
		return false;
	}
	
	// RESPONSE

	@Override
	public ResponseType[] getResponseTypes() {
		return new ResponseType[] {ResponseType.ENTITY_DAMAGE_BY_ENTITY, ResponseType.PLAYER_JOIN, ResponseType.PLAYER_QUIT, ResponseType.PLUGIN_DISABLE};
	}

	@Override
	public int getLevel(Event event) {
		if (event instanceof EntityDamageByEntityEvent) {
			return getLevel(((EntityDamageByEntityEvent) event).getDamager());
		} else if (event instanceof PlayerJoinEvent || event instanceof PlayerQuitEvent || event instanceof PluginDisableEvent) {
			return 0;
		} else {
			throw new IllegalArgumentException("Expected EntityDamageByEntityEvent, PlayerJoinEvent, PlayerQuitEvent or PluginDisableEvent, got: " + event);
		}
	}

	@Override
	public boolean zeroLevelFor(Class<? extends Response> response) {
		if (response == PlayerJoinResponse.class || response == PlayerQuitResponse.class || response == PluginDisableResponse.class) {
			return true;
		}
		return false;
	}

	@Override
	public int getLevel(Entity entity) {
		return ItemUtils.getMainHandLevel(entity, this);
	}
	
	// EFFECTS

	@Override
	public void onEvent(Event event, int level) {
		if (event instanceof EntityDamageByEntityEvent) {
			EntityDamageByEntityResponse.super.onEvent(event, level);
		} else if (event instanceof PlayerJoinEvent) {
			PlayerJoinResponse.super.onEvent(event, level);
		} else if (event instanceof PlayerQuitEvent) {
			PlayerQuitResponse.super.onEvent(event, level);
		} else if (event instanceof PluginDisableEvent) {
			PluginDisableResponse.super.onEvent(event, level);
		}
	}
	
	@Override
	public void onEntityDamageByEntity(EntityDamageByEntityEvent event, int level) {
		if (immuneEntities.contains(event.getEntityType())) {
			return;
		}
		Entity damager = event.getDamager();
		
		// players
		if (!settings.affectPlayers() && event.getEntity() instanceof Player && !Utils.isNPC(event.getEntity())) {
			return; // NPC is not a player
		}
		
		// CHECK FOR COOLDOWN
		if (cooldownHandler.isOnCooldown(damager)) {
			return;
		}
		
		// SUMMON
		int nr = summonAndSchedule(damager, damager.getLocation(), level);
		summoned.put(damager.getUniqueId(), System.currentTimeMillis());
		
		// MESSAGE
		if (settings.showSpawnMessage()) {
			damager.sendMessage(FE.getMessages().wolfSpawn(nr));
		}
		if (level != 0 && settings.getDurability(level) > 0) ItemUtils.handleMainHandDurability(damager, settings.getDurability(level));
		
		// COOLDOWN
		cooldownHandler.startCooldown(damager);
	}
	
	private int summonAndSchedule(final Entity owner, Location loc, int level) {
		int nr = settings.wolfsDefault() + level * settings.wolfsPerLevel();
		int health = settings.wolfHelathDefault() + level * settings.wolfHealthPerLevel();
		String suffix = settings.suffix();
		World world = loc.getWorld();
		
		final List<Wolf> wolfs = new ArrayList<>();
		for (int i = 0; i < nr; i++) {
			Wolf w = (Wolf) world.spawn(loc, Wolf.class, (wolf) -> {
				wolf.getAttribute(Attribute.GENERIC_MAX_HEALTH).setBaseValue(health);
				wolf.setHealth(health);
				if (owner instanceof AnimalTamer) {
					wolf.setOwner((AnimalTamer) owner);
				} else {
					FE.getLogger().warning("An entity that is unable to tame animals is using the " + getDisplayName() + " enchantment");
				}
				wolf.addScoreboardTag(METAKEY);
				wolf.setRemoveWhenFarAway(true);
				if (!suffix.isEmpty()) {
					wolf.setCustomName(ChatColor.RED + owner.getName() + ChatColor.AQUA + suffix);
					wolf.setCustomNameVisible(true);
				}
			});
			wolfs.add(w);
		}
		// schedule destruction
		destroy(wolfs, owner);
		return nr;
	}
	
	private void destroy(List<Wolf> wolfs, Entity owner) {
		FE.getServer().getScheduler().runTaskLater(FE, () -> {
			int nr = 0;
			for (Wolf w : wolfs) {
				if (w.isValid() && !w.isDead()) {
					w.remove();
					nr++;
				}
			}
			if (settings.showDespawnMessage() && owner.isValid() && nr > 1) {
				owner.sendMessage(FE.getMessages().wolfDespawn(nr));
			}
		}, 20L * settings.wolfLifeTime());
	}
	
	// COOLDOWN

	@Override
	public CooldownHandler getCooldownHandler() {
		return cooldownHandler;
	}
	
	// CLEANUP
	
	public void removeNearbyCustomWolves(Player player) {
		double wolfDist = 20;
		Location loc = player.getLocation();
		for (Entity e : loc.getWorld().getNearbyEntities(loc, wolfDist, wolfDist, wolfDist, (e) -> e instanceof Wolf)) {
			Wolf w = (Wolf) e; // predicate means they will be an instance of Wolf
			
			AnimalTamer owner = w.getOwner();
			if (owner == null || !owner.getUniqueId().equals(player.getUniqueId())) {
				continue;
			}

			// remove
			if (w.getScoreboardTags().contains(METAKEY)) {
				w.remove();
			}		
		}
	}

	@Override
	public void onPluginDisable(PluginDisableEvent event, int level) {
		if (event.getPlugin() != FE) {
			return;
		}
		for (Entry<UUID, Long> entry : summoned.entrySet()) {
			if (entry.getValue() + settings.wolfLifeTime() * 900L < System.currentTimeMillis()) {
				continue; 
			}
			Player player = FE.getServer().getPlayer(entry.getKey());
			if (player == null || !player.isOnline()) { // should have already removed
				continue;
			}
			removeNearbyCustomWolves(player);
		}
	}
	
	@Override
	public void onPlayerQuit(PlayerQuitEvent event, int level) {
		Player player = event.getPlayer();
		Long last = summoned.get(player.getUniqueId());
		if (last == null || last + settings.wolfLifeTime() * 900L < System.currentTimeMillis()) {
			return;
		}
		removeNearbyCustomWolves(player);
	}
	
	// for CRASHES
	@Override
	public void onPlayerJoin(PlayerJoinEvent event, int level) {
		removeNearbyCustomWolves(event.getPlayer());
	}
	
	// settings

	@Override
	public EnchantSettings getSettings() {
		return settings;
	}

}
