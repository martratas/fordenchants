package me.ford.fordenchants.enchants.weapon;

import java.util.EnumSet;
import java.util.Set;

import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import me.ford.fordenchants.FordEnchants;
import me.ford.fordenchants.enchants.level.LevelDependence.IgnoreZeroLevel;
import me.ford.fordenchants.enchants.responses.EntityDamageByEntityResponse;
import me.ford.fordenchants.settings.enchants.EnchantSettings;
import me.ford.fordenchants.settings.enchants.weapon.WeakenSettings;
import me.ford.fordenchants.utils.ItemUtils;
import me.ford.fordenchants.utils.Utils;
import net.md_5.bungee.api.ChatColor;

public class WeakenEnchant extends WeaponEnchant implements EntityDamageByEntityResponse, IgnoreZeroLevel {
	private static final String NAME = "weaken";
	private static final String DPNAME = ChatColor.GRAY + "Weaken";
	private static final Set<EntityType> immuneEntities = EnumSet.of(EntityType.ARMOR_STAND, EntityType.ITEM_FRAME, EntityType.LEASH_HITCH, EntityType.PAINTING,
			EntityType.MINECART, EntityType.MINECART_CHEST, EntityType.MINECART_COMMAND, EntityType.MINECART_FURNACE, EntityType.MINECART_HOPPER, EntityType.MINECART_MOB_SPAWNER, EntityType.MINECART_TNT);
	
	private final WeakenSettings settings;
	
	public WeakenEnchant(FordEnchants plugin) {
		super(plugin, NAME);
		settings = new WeakenSettings(FE);
	}

	@Override
	public String getName() {
		return NAME.toUpperCase();
	}

	@Override
	public String getDisplayName() {
		return DPNAME;
	}

	@Override
	public boolean conflictsWith(Enchantment ench) {
		return false;
	}

	@Override
	public int getMaxLevel() {
		return settings.maxLevel();
	}
	
	// RESPONSE

	@Override
	public ResponseType[] getResponseTypes() {
		return new ResponseType[] {ResponseType.ENTITY_DAMAGE_BY_ENTITY};
	}

	@Override
	public int getLevel(Event event) {
		if (event instanceof EntityDamageByEntityEvent) {
			return getLevel(((EntityDamageByEntityEvent) event).getDamager());
		}
		throw new IllegalArgumentException("Expected PlayerInteractAtEntityEvent, got: " + event);
	}

	@Override
	public int getLevel(Entity entity) {
		return ItemUtils.getMainHandLevel(entity, this);
	}
	
	// EFFECT

	@Override
	public void onEntityDamageByEntity(EntityDamageByEntityEvent event, int level) {
		if (immuneEntities.contains(event.getEntityType())) {
			return;
		}
		if (!(event.getEntity() instanceof LivingEntity)) {
			return;
		}
		Entity damager = event.getDamager();
		
		// players
		if (!settings.affectPlayers() && event.getEntity() instanceof Player && !Utils.isNPC(event.getEntity())) {
			return; // NPCs are not players
		}
		
		// chance
		double chance = settings.defaultChance() + level * settings.chancePerLevel();
		double nr = FE.getRandomProvider().nextDouble();
		if (chance < nr) {
			return;
		}
		
		double duration = weaken((LivingEntity) event.getEntity(), level);
		if (settings.sendMessage()) {
			damager.sendMessage(settings.getMessage(FE.getMessages(), level, duration));
		}
		if (level != 0 && settings.getDurability(level) > 0) ItemUtils.handleMainHandDurability(damager, settings.getDurability(level));
	}
	
	private double weaken(LivingEntity entity, int level) {
		int effectLevel = settings.defaultLevel() + level * settings.levelPerLevel();
		double duration = settings.defaultDuration() + level * settings.durationPerLevel();
		int durTicks = (int) Math.floor(duration * 20);
		PotionEffect effect = new PotionEffect(PotionEffectType.WEAKNESS, effectLevel, durTicks);
		entity.addPotionEffect(effect, true);
		return duration;
	}
	
	// settings

	@Override
	public EnchantSettings getSettings() {
		return settings;
	}

}
