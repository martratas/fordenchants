package me.ford.fordenchants.enchants.crossbow;

import org.bukkit.ChatColor;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Entity;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

import me.ford.fordenchants.FordEnchants;
import me.ford.fordenchants.enchants.cooldowns.CooldownEnchant;
import me.ford.fordenchants.enchants.cooldowns.CooldownHandler;
import me.ford.fordenchants.enchants.level.LevelDependence.IgnoreZeroLevel;
import me.ford.fordenchants.enchants.responses.EntityDamageByEntityResponse;
import me.ford.fordenchants.settings.enchants.EnchantSettings;
import me.ford.fordenchants.settings.enchants.crossbow.TagEmSettings;
import me.ford.fordenchants.utils.DisarmNeutralizeUtils;
import me.ford.fordenchants.utils.ItemUtils;
import me.ford.fordenchants.utils.TagEmMarkEmUtils;

public class TagEmEnchant extends CrossbowEnchant implements EntityDamageByEntityResponse, IgnoreZeroLevel, CooldownEnchant {
	private static final String NAME = "tag_em";
	private static final String DPNAME = ChatColor.GRAY + "Tag 'Em";
	private final TagEmSettings settings;
	private final CooldownHandler cooldown;

	public TagEmEnchant(FordEnchants plugin) {
		super(plugin, NAME);
		settings = new TagEmSettings(FE);
		cooldown = new CooldownHandler(settings.cooldown());
	}

	@Override
	public String getDisplayName() {
		return DPNAME;
	}

	@Override
	public String getName() {
		return NAME.toUpperCase();
	}

	@Override
	public int getMaxLevel() {
		return settings.maxLevel();
	}

	@Override
	public boolean conflictsWith(Enchantment other) {
		return false;
	}
	
	// RESPONSE

	@Override
	public ResponseType[] getResponseTypes() {
		return new ResponseType[] {ResponseType.ENTITY_DAMAGE_BY_ENTITY};
	}

	@Override
	public int getLevel(Event event) {
		if ((event instanceof EntityDamageByEntityEvent)) {
			Entity source = DisarmNeutralizeUtils.getProjectileSource((EntityDamageByEntityEvent) event);
			if (source != null) {
				return getLevel(source);
			}
			return 0;
		}
		throw new IllegalArgumentException("Expected EntityDamageByEntityEvent, got: " + event);
	}

	@Override
	public int getLevel(Entity entity) {
		return ItemUtils.getCrossbowLevel(entity, this);
	}
	
	// EFFECT

	@Override
	public void onEntityDamageByEntity(EntityDamageByEntityEvent event, int level) {
		TagEmMarkEmUtils.onEntityDamageByEntity(event, level, cooldown, settings, FE);
		if (level != 0 && settings.getDurability(level) > 0) ItemUtils.handleCrossbowDurability(DisarmNeutralizeUtils.getProjectileSource(event), settings.getDurability(level));
	}
	
	// COOLDOWN

	@Override
	public CooldownHandler getCooldownHandler() {
		return cooldown;
	}
	
	// settings

	@Override
	public EnchantSettings getSettings() {
		return settings;
	}

}
