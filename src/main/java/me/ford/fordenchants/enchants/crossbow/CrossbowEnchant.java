package me.ford.fordenchants.enchants.crossbow;

import org.bukkit.enchantments.EnchantmentTarget;
import org.bukkit.inventory.ItemStack;

import me.ford.fordenchants.FordEnchants;
import me.ford.fordenchants.enchants.FordEnchant;
import me.ford.fordenchants.utils.ItemUtils;

public abstract class CrossbowEnchant extends FordEnchant {

	public CrossbowEnchant(FordEnchants plugin, String name) {
		super(plugin, name);
	}

	@Override
	public boolean canEnchantItem(ItemStack item) {
		return ItemUtils.isCrossbow(item);
	}

	@Override
	public EnchantmentTarget getItemTarget() {
		return EnchantmentTarget.CROSSBOW;
	}

}
