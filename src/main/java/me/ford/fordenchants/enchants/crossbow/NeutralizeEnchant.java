package me.ford.fordenchants.enchants.crossbow;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.ChatColor;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.inventory.ItemStack;

import me.ford.fordenchants.FordEnchants;
import me.ford.fordenchants.enchants.cooldowns.CooldownEnchant;
import me.ford.fordenchants.enchants.cooldowns.CooldownHandler;
import me.ford.fordenchants.enchants.level.LevelDependence.IgnoreZeroLevel;
import me.ford.fordenchants.enchants.responses.EntityDamageByEntityResponse;
import me.ford.fordenchants.enchants.responses.EntityDeathResponse;
import me.ford.fordenchants.enchants.responses.Response;
import me.ford.fordenchants.enchants.responses.ZeroLevelResponse;
import me.ford.fordenchants.settings.enchants.EnchantSettings;
import me.ford.fordenchants.settings.enchants.crossbow.NeutralizeSettings;
import me.ford.fordenchants.utils.DisarmNeutralizeUtils;
import me.ford.fordenchants.utils.ItemUtils;

public class NeutralizeEnchant extends CrossbowEnchant implements EntityDamageByEntityResponse, IgnoreZeroLevel, CooldownEnchant, EntityDeathResponse, ZeroLevelResponse {
	private static final String NAME = "neutralize";
	private static final String DPNAME = ChatColor.GRAY + "Neutralize";
	private final NeutralizeSettings settings;
	private final CooldownHandler cooldown;
	private final Map<LivingEntity, ItemStack> disarmed = new HashMap<>();
	
	public NeutralizeEnchant(FordEnchants plugin) {
		super(plugin, NAME);
		settings = new NeutralizeSettings(FE);
		cooldown = new CooldownHandler(settings.cooldown());
	}

	@Override
	public String getDisplayName() {
		return DPNAME;
	}

	@Override
	public String getName() {
		return NAME.toUpperCase();
	}

	@Override
	public int getMaxLevel() {
		return settings.maxLevel();
	}

	@Override
	public boolean conflictsWith(Enchantment other) {
		return false;
	}
	
	// RESPONSE

	@Override
	public ResponseType[] getResponseTypes() {
		return new ResponseType[] {ResponseType.ENTITY_DAMAGE_BY_ENTITY};
	}

	@Override
	public int getLevel(Event event) {
		if ((event instanceof EntityDamageByEntityEvent)) {
			Entity source = DisarmNeutralizeUtils.getProjectileSource((EntityDamageByEntityEvent) event);
			if (source != null) {
				return getLevel(source);
			}
			return 0;
		} else if (event instanceof EntityDeathEvent) {
			return 0;
		}
		throw new IllegalArgumentException("Expected EntityDamageByEntityEvent or EntityDeathEvent, got: " + event);
	}

	@Override
	public int getLevel(Entity entity) {
		return ItemUtils.getCrossbowLevel(entity, this);
	}

	@Override
	public boolean zeroLevelFor(Class<? extends Response> response) {
		if (response == EntityDeathResponse.class) {
			return true;
		}
		return false;
	}
	
	// EFFECT

	@Override
	public void onEvent(Event event, int level) {
		if (event instanceof EntityDamageByEntityEvent) {
			EntityDamageByEntityResponse.super.onEvent(event, level);
		} else if (event instanceof EntityDeathEvent) {
			EntityDeathResponse.super.onEvent(event, level);
		} else {
			throw new IllegalArgumentException("Expected EntityDamageByEntityEvent or EntityDeathEvent, got " + event);
		}
	}

	@Override
	public void onEntityDamageByEntity(EntityDamageByEntityEvent event, int level) {
		DisarmNeutralizeUtils.onEntityDamageByEntity(event, level, cooldown, settings, FE, disarmed);
		if (level != 0 && settings.getDurability(level) > 0) ItemUtils.handleCrossbowDurability(DisarmNeutralizeUtils.getProjectileSource(event), settings.getDurability(level));
	}

	// death while disarmed
	
	@Override
	public void onEntityDeath(EntityDeathEvent event, int level) {
		DisarmNeutralizeUtils.onEntityDeath(event, settings, FE, disarmed);
	}
	
	// COOLDOWN

	@Override
	public CooldownHandler getCooldownHandler() {
		return cooldown;
	}
	
	// settings

	@Override
	public EnchantSettings getSettings() {
		return settings;
	}

}
