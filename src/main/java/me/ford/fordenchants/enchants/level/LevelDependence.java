package me.ford.fordenchants.enchants.level;

import org.bukkit.entity.Entity;
import org.bukkit.event.Event;

public interface LevelDependence {
	
	public int getLevel(Event event);
	
	public int getLevel(Entity entity);
	
	public boolean ignoreZeroLevel();
	
	public static interface IgnoreZeroLevel extends LevelDependence {
		
		@Override
		public default boolean ignoreZeroLevel() {
			return true;
		}
	}
	
	public static interface EnforceZeroLevel extends LevelDependence {
		
		@Override
		public default boolean ignoreZeroLevel() {
			return false;
		}
	}

}
