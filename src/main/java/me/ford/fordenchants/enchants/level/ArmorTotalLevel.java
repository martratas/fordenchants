package me.ford.fordenchants.enchants.level;

import org.bukkit.entity.Entity;

import me.ford.fordenchants.enchants.FordEnchant;
import me.ford.fordenchants.utils.ItemUtils;

public interface ArmorTotalLevel extends LevelDependence {
	
	@Override
	public default int getLevel(Entity entity) {
		int levels = 0;
		for (int i : ItemUtils.getArmorLevel(entity, (FordEnchant) this).values()) {
			levels += i;
		}
		return levels;
	}
	
}
