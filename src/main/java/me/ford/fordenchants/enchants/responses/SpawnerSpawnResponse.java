package me.ford.fordenchants.enchants.responses;

import org.bukkit.event.Event;
import org.bukkit.event.entity.SpawnerSpawnEvent;

public interface SpawnerSpawnResponse extends Response {
	
	public void onSpawnerSpawn(SpawnerSpawnEvent event, int levels);
	
	public default void onEvent(Event event, int level) {
		if (!(event instanceof SpawnerSpawnEvent)) {
			throw new IllegalArgumentException("Expected SpawnerSpawnEvent,  got:" + event);
		}
		onSpawnerSpawn((SpawnerSpawnEvent) event, level);
	}

}
