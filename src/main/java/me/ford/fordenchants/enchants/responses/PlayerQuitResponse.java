package me.ford.fordenchants.enchants.responses;

import org.bukkit.event.Event;
import org.bukkit.event.player.PlayerQuitEvent;

public interface PlayerQuitResponse extends Response {
	
	public void onPlayerQuit(PlayerQuitEvent event, int level);
	
	public default void onEvent(Event event, int level) {
		if (!(event instanceof PlayerQuitEvent)) {
			throw new IllegalArgumentException("Expected PlayerQuitEvent,  got:" + event);
		}
		onPlayerQuit((PlayerQuitEvent) event, level);
	}

}
