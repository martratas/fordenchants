package me.ford.fordenchants.enchants.responses;

import org.bukkit.event.Event;
import org.bukkit.event.server.PluginDisableEvent;

public interface PluginDisableResponse extends Response {
	
	public void onPluginDisable(PluginDisableEvent event, int level);
	
	public default void onEvent(Event event, int level) {
		if (!(event instanceof PluginDisableEvent)) {
			throw new IllegalArgumentException("Expected PluginDisableEvent,  got:" + event);
		}
		onPluginDisable((PluginDisableEvent) event, level);
	}

}
