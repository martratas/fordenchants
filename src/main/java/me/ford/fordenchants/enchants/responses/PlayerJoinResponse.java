package me.ford.fordenchants.enchants.responses;

import org.bukkit.event.Event;
import org.bukkit.event.player.PlayerJoinEvent;

public interface PlayerJoinResponse extends Response {
	
	public void onPlayerJoin(PlayerJoinEvent event, int level);
	
	public default void onEvent(Event event, int level) {
		if (!(event instanceof PlayerJoinEvent)) {
			throw new IllegalArgumentException("Expected PlayerJoinEvent,  got:" + event);
		}
		onPlayerJoin((PlayerJoinEvent) event, level);
	}

}
