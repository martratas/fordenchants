package me.ford.fordenchants.enchants.responses;

import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityDeathEvent;

public interface EntityDeathResponse extends Response {
	
	public void onEntityDeath(EntityDeathEvent event, int level);
	
	public default void onEvent(Event event, int level) {
		if (!(event instanceof EntityDeathEvent)) {
			throw new IllegalArgumentException("Expected EntityDeathEvent,  got:" + event);
		}
		onEntityDeath((EntityDeathEvent) event, level);
	}

}
