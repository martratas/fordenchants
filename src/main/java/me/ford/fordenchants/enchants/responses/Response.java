package me.ford.fordenchants.enchants.responses;

import org.bukkit.event.Event;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityTargetLivingEntityEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.SpawnerSpawnEvent;
import org.bukkit.event.inventory.FurnaceSmeltEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerChangedMainHandEvent;
import org.bukkit.event.server.PluginDisableEvent;

public interface Response {
	
	public void onEvent(Event event, int level);
	
	public ResponseType[] getResponseTypes();
	
	public static enum ResponseType {
		ENTITY_DAMAGE_BY_ENTITY(EntityDamageByEntityResponse.class, EntityDamageByEntityEvent.class),
		ENTITY_TARGET_LIVING_ENTITY(EntityTargetLivingEntityResponse.class, EntityTargetLivingEntityEvent.class),
		PLAYER_JOIN(PlayerJoinResponse.class, PlayerJoinEvent.class),
		PLAYER_QUIT(PlayerQuitResponse.class, PlayerQuitEvent.class),
		PLUGIN_DISABLE(PluginDisableResponse.class, PluginDisableEvent.class),
		SPAWNER_SPAWN(SpawnerSpawnResponse.class, SpawnerSpawnEvent.class),
		PLAYER_MOVE(PlayerMoveResponse.class, PlayerMoveEvent.class),
		FURNACE_SMELT(FurnaceSmeltResponse.class, FurnaceSmeltEvent.class),
		PLAYER_INTERACT(PlayerInteractResponse.class, PlayerInteractEvent.class),
		ENTITY_DEATH(EntityDeathResponse.class, EntityDeathEvent.class),
		BLOCK_BREAK(BlockBreakResponse.class, BlockBreakEvent.class),
		PLAYER_CHANGED_MAIN_HAND(PlayerChangedMainHandResponse.class, PlayerChangedMainHandEvent.class);
		
		private final Class<? extends Response> responseClass;
		private final Class<? extends Event> eventClass;
		
		ResponseType(Class<? extends Response> responseClass, Class<? extends Event> eventClass) {
			this.responseClass = responseClass;
			this.eventClass = eventClass;
		}
		
		public Class<? extends Response> getResponseClass() {
			return responseClass;
		}
		
		public Class<? extends Event> getEventClass() {
			return eventClass;
		}
		
		public static Class<? extends Response> getResponseClass(Class<? extends Event> clazz) {
			for (ResponseType type : values()) {
				if (type.getEventClass().equals(clazz)) {
					return type.getResponseClass();
				}
			}
			return null;
		}
	}

}
