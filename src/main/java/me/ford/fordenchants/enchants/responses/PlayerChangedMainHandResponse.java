package me.ford.fordenchants.enchants.responses;

import org.bukkit.event.Event;
import org.bukkit.event.player.PlayerChangedMainHandEvent;

/**
 * PlayerChangedMainHandResponse
 */
public interface PlayerChangedMainHandResponse extends Response {
	
	public void onPlayerInteract(PlayerChangedMainHandEvent event, int level);
	
	public default void onEvent(Event event, int level) {
		if (!(event instanceof PlayerChangedMainHandEvent)) {
			throw new IllegalArgumentException("Expected PlayerChangedMainHandEvent,  got:" + event);
		}
		onPlayerInteract((PlayerChangedMainHandEvent) event, level);
	}

    
}