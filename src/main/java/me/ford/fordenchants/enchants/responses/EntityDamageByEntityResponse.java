package me.ford.fordenchants.enchants.responses;

import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

public interface EntityDamageByEntityResponse extends Response {
	
	public void onEntityDamageByEntity(EntityDamageByEntityEvent event, int level);
	
	public default void onEvent(Event event, int level) {
		if (!(event instanceof EntityDamageByEntityEvent)) {
			throw new IllegalArgumentException("Expected EntityDamageByEntityEvent,  got:" + event);
		}
		onEntityDamageByEntity((EntityDamageByEntityEvent) event, level);
	}
	
}
