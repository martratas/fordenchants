package me.ford.fordenchants.enchants.responses;

import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityTargetLivingEntityEvent;

public interface EntityTargetLivingEntityResponse extends Response {
	
	public void onEntityTargetLivingEntity(EntityTargetLivingEntityEvent event, int level);
	
	public default void onEvent(Event event, int level) {
		if (!(event instanceof EntityTargetLivingEntityEvent)) {
			throw new IllegalArgumentException("Expected EntityTargetLivingEntityEvent,  got:" + event);
		}
		onEntityTargetLivingEntity((EntityTargetLivingEntityEvent) event, level);
	}

}
