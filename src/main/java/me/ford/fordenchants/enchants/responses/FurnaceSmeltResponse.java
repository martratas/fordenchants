package me.ford.fordenchants.enchants.responses;

import org.bukkit.event.Event;
import org.bukkit.event.inventory.FurnaceSmeltEvent;

public interface FurnaceSmeltResponse extends Response {
	
	public void onFurnaceSmelt(FurnaceSmeltEvent event, int level);
	
	public default void onEvent(Event event, int level) {
		if (!(event instanceof FurnaceSmeltEvent)) {
			throw new IllegalArgumentException("Expected FurnaceSmeltEvent,  got:" + event);
		}
		onFurnaceSmelt((FurnaceSmeltEvent) event, level);
	}

}
