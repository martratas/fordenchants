package me.ford.fordenchants.enchants.responses;

import org.bukkit.event.Event;
import org.bukkit.event.player.PlayerMoveEvent;

public interface PlayerMoveResponse extends Response {
	
public void onPlayerMove(PlayerMoveEvent event, int level);
	
	public default void onEvent(Event event, int level) {
		if (!(event instanceof PlayerMoveEvent)) {
			throw new IllegalArgumentException("Expected PlayerMoveEvent,  got:" + event);
		}
		onPlayerMove((PlayerMoveEvent) event, level);
	}

}
