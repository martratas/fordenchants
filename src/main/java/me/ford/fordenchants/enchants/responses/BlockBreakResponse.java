package me.ford.fordenchants.enchants.responses;

import org.bukkit.event.Event;
import org.bukkit.event.block.BlockBreakEvent;

public interface BlockBreakResponse extends Response {
	
	public void onBlockBreak(BlockBreakEvent event, int level);
	
	public default void onEvent(Event event, int level) { 
		if (event instanceof BlockBreakEvent) {
			onBlockBreak((BlockBreakEvent) event, level);
		} else {
			throw new IllegalArgumentException("Expected BlockBreakEvent, got " + event);
		}
	}

}
