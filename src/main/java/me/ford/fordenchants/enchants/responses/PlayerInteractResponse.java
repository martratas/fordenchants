package me.ford.fordenchants.enchants.responses;

import org.bukkit.event.Event;
import org.bukkit.event.player.PlayerInteractEvent;

public interface PlayerInteractResponse extends Response {
	
	public void onPlayerInteract(PlayerInteractEvent event, int level);
	
	public default void onEvent(Event event, int level) {
		if (!(event instanceof PlayerInteractEvent)) {
			throw new IllegalArgumentException("Expected PlayerInteractEvent,  got:" + event);
		}
		onPlayerInteract((PlayerInteractEvent) event, level);
	}

}
