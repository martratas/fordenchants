package me.ford.fordenchants.enchants.responses;

/**
 * PlayerMoveBlockResponse
 * 
 * Only triggered when the player has moved an entire block
 */
public interface PlayerMoveBlockResponse extends PlayerMoveResponse {

    
}