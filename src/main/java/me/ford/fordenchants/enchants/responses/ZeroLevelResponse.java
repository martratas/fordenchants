package me.ford.fordenchants.enchants.responses;

public interface ZeroLevelResponse extends Response {
	
	public boolean zeroLevelFor(Class<? extends Response> response);

}
