package me.ford.fordenchants.enchants;

import org.bukkit.NamespacedKey;
import org.bukkit.enchantments.Enchantment;

import me.ford.fordenchants.FordEnchants;
import me.ford.fordenchants.settings.enchants.EnchantSettings;
import net.md_5.bungee.api.ChatColor;

public abstract class FordEnchant extends Enchantment {
	protected final NamespacedKey key;
	protected final FordEnchants FE;

	public FordEnchant(FordEnchants plugin, String name) {
		super(new NamespacedKey(plugin, name));
		this.key = super.getKey();
		FE = plugin;
	}
	
	public abstract String getDisplayName();

	@Override
	public int getStartLevel() {
		return 0;
	}

	@Override
	public boolean isCursed() {
		return false;
	}

	@Override
	public boolean isTreasure() {
		return false;
	}
	
	@Override
	public String toString() {
		return getDisplayName() + ChatColor.RESET;
	}
	
	public abstract EnchantSettings getSettings();
}
