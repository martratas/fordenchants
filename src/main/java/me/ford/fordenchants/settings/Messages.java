package me.ford.fordenchants.settings;

import org.bukkit.inventory.ItemStack;

import me.ford.fordenchants.FordEnchants;
import me.ford.fordenchants.enchants.FordEnchant;
import me.ford.fordenchants.utils.Utils;
import net.md_5.bungee.api.ChatColor;

public class Messages {
	private static final String PATH = "messages.";
	private final FordEnchants FE;
	
	public Messages(FordEnchants plugin) {
		FE = plugin;
	}
	
	public String getMessage(String path, String def, String... args) {
		String msg = FE.getConfig().getString(PATH + path, def);
		for (int i = 0; i < args.length/2; i++) {
			String from = args[2*i];
			String to = args[2*i + 1];
			msg = msg.replace(from, to);
		}
		return ChatColor.translateAlternateColorCodes('&', msg);
	}
	
	// from enchant command
	
	public String onlyPlayers() {
		return getMessage("only-players", "&cOnly players can use this command");
	}
	
	public String enchantNotFound(String name) {
		return getMessage("enchant-not-found", "&cEnchant not found: &7{enchant}", "{enchant}", name);
	}
	
	public String unsupportedLevel(int level, int max) {
		return getMessage("unsupported-level", "&cUnsupported level: &7{level} &c(max: &8{max}&c)", 
						"{level}", String.valueOf(level), "{max}", String.valueOf(max));
	}
	
	public String needItemInHand() {
		return getMessage("need-item-in-hand", "&cNeed an item in hand!");
	}
	
	public String addedEnchantment(FordEnchant enchant, int level) {
		return getMessage("added-enchant", "&6Added the enchantment &7{enchant}&6 of level &8{level}",
						"{enchant}", ChatColor.stripColor(enchant.getDisplayName()), "{level}", String.valueOf(level));  
	}
	
	public String removedEnchantment(FordEnchant enchant) {
		return getMessage("removed-enchant", "&6Removed the enchantment &7{enchant}&6",
						"{enchant}", ChatColor.stripColor(enchant.getDisplayName()));  
	}
	
	public String reloadedMessage() {
		return getMessage("reloaded", "&cSuccessfully reloaded the config!");
	}
	
	public String enchantmentNotApplicable(ItemStack item, FordEnchant enchant) {
		String msg = getMessage("enchant-not-applicable", "&cThe enchant &7{enchant}&c is not applicable for item of type &8{type}");
		msg = msg.replace("{type}", item.getType().name()).replace("{enchant}", ChatColor.stripColor(enchant.getDisplayName()));
		return msg;
	}
	
	// ENCHANT messages
	
	// Summon Wolf enchant
	
	public String wolfSpawn(int nr) {
		String plural = nr == 1 ? "wolf" : "wolves";
		return getMessage("enchants.summon-wolf.wolf-spawn", "&6Spawned &7{nr}&6 wolf to help you!", "wolf", plural, "{nr}", String.valueOf(nr));
	}
	
	public String wolfDespawn(int nr) {
		String plural = nr == 1 ? "wolf" : "wolves";
		return getMessage("enchants.summon-wolf.wolf-despawn", "&6The wolf got tired!", "wolf", plural);
	}
	
	// Weaken enchant
	
	public String weakened(int level, double duration) {
		return getMessage("enchants.weaken.weakened", "&6Weaken level &7{level}&6 applied for &8{time}&6 seconds",
							"{level}", String.valueOf(level), "{time}", Utils.formatDouble(duration, 2));
	}
	
	// Pull enchant
	
	public String pulled(int level) {
		return getMessage("enchants.pull.pulled", "&6Pulled target!", "{level}", String.valueOf(level));
	}
	
	// Damage Spread enchant
	
	public String damageSpread(int nr) {
		return getMessage("enchants.damage-spread.spread", "&6Damage spread to &7{nr}&6 nearby mobs!", "{nr}", String.valueOf(nr));
	}
	
	// Levitate enchant
	
	public String levitated(int level, double duration) {
		return getMessage("enchants.levitate.levitated", "&6Levitation level &7{level}&6 applied for &8{time}&6 seconds",
							"{level}", String.valueOf(level), "{time}", Utils.formatDouble(duration, 2));
	}
	
	// Disarm enchant
	
	public String disarmed(int level) {
		return getMessage("enchants.disarm.disarmed", "&6Disarmed the enemy!", "{level}", String.valueOf(level));
	}
	
	// Neutralize enchant
	
	public String neutralized(int level) {
		return getMessage("enchants.neutralize.neutralized", "&6Neutralized the enemy!", "{level}", String.valueOf(level));
	}
	
	// Mark Em enchant
	
	public String marked(double duration) {
		return getMessage("enchants.mark-em.marked", "&6Enemy marked for &7{time}&6 seconds!", "{time}", Utils.formatDouble(duration, 2));
	}
	
	// Tag Em enchant
	
	public String tagged(double duration) {
		return getMessage("enchants.tag-em.tagged", "&6Enemy tagged for &7{time}&6 seconds!", "{time}", Utils.formatDouble(duration, 2));
	}
	
	// Leap enchant
	
	public String leapt(double distance) {
		return getMessage("enchants.leap.leapt", "&6Leapt &7{dist}&6 blocks!", "{dist}", Utils.formatDouble(distance, 2));
	}
	
	// Magnet
	
	public String full() {
		return getMessage("enchants.magnet.full", "&6Cannot pull items to inventory, not enough room!");
	}

}
