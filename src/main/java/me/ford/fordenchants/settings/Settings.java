package me.ford.fordenchants.settings;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import org.bukkit.configuration.ConfigurationSection;

import me.ford.fordenchants.FordEnchants;

public class Settings {
	private static final String PATH = "settings";
	private static final String ENCHANTS_PATH = "enchants-at-enchanting-table";
	private final FordEnchants FE;
	private boolean nagged = false;
	
	public Settings(FordEnchants plugin) {
		FE = plugin;
	}
	
	private ConfigurationSection get() {
		return FE.getConfig().getConfigurationSection(PATH);
	}
	
	private ConfigurationSection getEnchants() {
		return get().getConfigurationSection(ENCHANTS_PATH);
	}
	
	public boolean addLevelWhenEqual() {
		return get().getBoolean("add-level-when-equal", true);
	}
	
	public boolean simpleAnvilLevels() {
		return get().getBoolean("simple-anvil-levels", false);
	}
	
	// enchants at enchanting table
	
	public boolean useEnchantingTable() {
		return getEnchants().getBoolean("enabled", true);
	}
	
	// one
	
	public int minimumForOne() {
		return getEnchants().getInt("chance-for-one.starting-level", 25);
	}
	
	public double chanceForOneStart() {
		return getEnchants().getDouble("chance-for-one.at-start", 0.5);
	}
	
	public double chanceForOnePerLevel() {
		return getEnchants().getDouble("chance-for-one.per-level", 0.25);
	}
	
	public double chanceForOne(int level) {
		return chanceForOneStart() + (level - minimumForOne()) * chanceForOnePerLevel();
	}
	
	// two
	
	public int minimumForTwo() {
		return getEnchants().getInt("chance-for-two.starting-level", 25);
	}
	
	public double chanceForTwoStart() {
		return getEnchants().getDouble("chance-for-two.at-start", 0.5);
	}
	
	public double chanceForTwoPerLevel() {
		return getEnchants().getDouble("chance-for-two.per-level", 0.25);
	}
	
	public double chanceForTwo(int level) {
		return chanceForTwoStart() + (level - minimumForTwo()) * chanceForTwoPerLevel();
	}
	
	// three
	
	public int minimumForThree() {
		return getEnchants().getInt("chance-for-three.starting-level", 25);
	}
	
	public double chanceForThreeStart() {
		return getEnchants().getDouble("chance-for-three.at-start", 0.5);
	}
	
	public double chanceForThreePerLevel() {
		return getEnchants().getDouble("chance-for-three.per-level", 0.25);
	}
	
	public double chanceForThree(int level) {
		return chanceForThreeStart() + (level - minimumForThree()) * chanceForThreePerLevel();
	}
	
	public int getNumberOfCustomEnchantsForLevel(int enchantingLevel) {
		int nr = 0;
		if (!useEnchantingTable()) {
			return nr;
		}
		// in case the chances have been changed and are not between 0 and 1
		double max = Math.max(Math.max(chanceForThree(enchantingLevel), chanceForTwo(enchantingLevel)), chanceForOne(enchantingLevel));
		double min = Math.min(Math.min(chanceForThree(enchantingLevel), chanceForTwo(enchantingLevel)), chanceForOne(enchantingLevel));
		max = Math.max(1, max);
		min = Math.min(0, min);
		if (max != 1 && !nagged) {
			FE.getLogger().warning("Expected minimum chance to be 0 or more and maximum chance to be 1 or less for number of custom enchants." +
					" Got " + min + " for min and " + max + " for max instead." +
					" Trying to cope as best possible");
			nagged = true;
		}
		
		double rnd = FE.getRandomProvider().nextDouble() * max; //range + min; if it's negative, I'll assume it's never wanted
		if (enchantingLevel >= minimumForThree() && rnd < chanceForThree(enchantingLevel)) {
			return 3;
		}
		if (enchantingLevel >= minimumForTwo() && rnd < chanceForTwo(enchantingLevel)) {
			return 2;
		}
		if (enchantingLevel >= minimumForOne() && rnd < chanceForOne(enchantingLevel)) {
			return 1;
		}
		return 0;
	}
	
	// chance for level
	
	// level 1
	
	public ConfigurationSection levelOne() {
		return getEnchants().getConfigurationSection("chance-for-level.1");
	}
	
	// level 2
	
	public ConfigurationSection levelTwo() {
		return getEnchants().getConfigurationSection("chance-for-level.2");
	}
	
	// level 3
	
	public ConfigurationSection levelThree() {
		return getEnchants().getConfigurationSection("chance-for-level.3");
	}
	
	// level 4
	
	public ConfigurationSection levelFour() {
		return getEnchants().getConfigurationSection("chance-for-level.4");
	}
	
	// level 5
	
	public ConfigurationSection levelFive() {
		return getEnchants().getConfigurationSection("chance-for-level.5");
	}
	
	// getter for specific level
	
	public double chanceForLevel(ConfigurationSection section, int levelDiff) {
		return section.getDouble("at-start") + levelDiff * section.getDouble("per-level"); 
	}
	
	public int getLevelForCustomEnchantForLevel(int enchantingLevel, int maxLevel) {
		Map<Integer, Double> levels = new HashMap<>();
		levels.put(1, chanceForLevel(levelOne(), enchantingLevel - minimumForOne()));
		if (maxLevel > 1) levels.put(2, chanceForLevel(levelTwo(), enchantingLevel - minimumForOne()));
		if (maxLevel > 2) levels.put(3, chanceForLevel(levelThree(), enchantingLevel - minimumForOne()));
		if (maxLevel > 3) levels.put(4, chanceForLevel(levelFour(), enchantingLevel - minimumForOne()));
		if (maxLevel > 4) levels.put(5, chanceForLevel(levelFive(), enchantingLevel - minimumForOne()));
		// in case there were changes and the chances are not below 1
		double max = 1;
		double min = 0;
		for (double val : levels.values()) {
			if (val > max) {
				max = val;
			}
			if (val < min) {
				min = val;
			}
		}
		
		Map<Integer, Double> sorted = levels.entrySet().stream().sorted((e1,e2) -> (e2.getValue().compareTo(e1.getValue()))).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1,e2) -> e1, LinkedHashMap::new));
		
		double rnd = FE.getRandomProvider().nextDouble();
		if (max != 1 && !nagged) {
			FE.getLogger().warning("Expected minimum chance to be 0 or more and maximum chance to be 1 or less for getting level of enchants." +
				" Got " + min + " for min and " + max + " for max instead." +
				" Trying to cope as best possible");
			rnd *= max;
			nagged = true;
		}
		for (Map.Entry<Integer, Double> entry : sorted.entrySet()) {
			if (rnd < entry.getValue()) return entry.getKey();
		}
		return 0;
	}

}
