package me.ford.fordenchants.settings.enchants.crossbow;

import me.ford.fordenchants.FordEnchants;
import me.ford.fordenchants.settings.Messages;
import me.ford.fordenchants.settings.enchants.DisarmNeutralizeBaseSettings;

public class NeutralizeSettings extends DisarmNeutralizeBaseSettings {
	private static final String PATH = "enchants.neutralize";

	public NeutralizeSettings(FordEnchants plugin) {
		super(plugin);
	}

	@Override
	public String getPath() {
		return PATH;
	}
	
	@Override
	public String getMessage(Messages messages, int level) {
		return messages.neutralized(level);
	}

}
