package me.ford.fordenchants.settings.enchants.armor;

import me.ford.fordenchants.FordEnchants;
import me.ford.fordenchants.settings.enchants.EnchantSettings;
import me.ford.fordenchants.settings.enchants.IEnchantWithDurabilitySettings;

public class FurnaceEnhanceSettings extends EnchantSettings implements IEnchantWithDurabilitySettings {
	private static final String PATH = "enchants.furnace-enhance";
	
	public FurnaceEnhanceSettings(FordEnchants plugin) {
		super(plugin);
	}

	@Override
	public String getPath() {
		return PATH;
	}
	
	public double distance() {
		return getDouble("distance", 4.0);
	}
	
	public double defaultSpeedup() {
		return getDouble("speedup-default", 2.0);
	}
	
	public double speedupPerLevel() {
		return getDouble("speedup-per-level", 0.5);
	}

}
