package me.ford.fordenchants.settings.enchants.weapon;

import me.ford.fordenchants.FordEnchants;
import me.ford.fordenchants.settings.Messages;
import me.ford.fordenchants.settings.enchants.EnchantSettings;
import me.ford.fordenchants.settings.enchants.IEnchantWithChanceSettings;
import me.ford.fordenchants.settings.enchants.IEnchantWithDurabilitySettings;
import me.ford.fordenchants.settings.enchants.IEnchantWithMessageSettings;

public class LeapSettings extends EnchantSettings implements IEnchantWithChanceSettings, IEnchantWithMessageSettings, IEnchantWithDurabilitySettings {
	private static final String PATH = "enchants.leap";

	public LeapSettings(FordEnchants plugin) {
		super(plugin);
	}

	@Override
	public String getPath() {
		return PATH;
	}
	
	public int cooldown() {
		return getInt("cooldown", 120);
	}
	
	public double defaultVelocity() {
		return getDouble("default-velocity", 0.5);
	}
	
	public double velocityPerLevel() {
		return getDouble("velocity-per-level", 0.1);
	}
	
	public String getMessage(Messages messages, double dist) {
		return messages.leapt(dist);
	}

}
