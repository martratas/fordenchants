package me.ford.fordenchants.settings.enchants.bow;

import me.ford.fordenchants.FordEnchants;
import me.ford.fordenchants.settings.Messages;
import me.ford.fordenchants.settings.enchants.MarkEmTagEmBaseSettings;

public class MarkEmSettings extends MarkEmTagEmBaseSettings {
	private static final String PATH = "enchants.mark-em";

	public MarkEmSettings(FordEnchants plugin) {
		super(plugin);
	}

	@Override
	public String getPath() {
		return PATH;
	}

	@Override
	public String getMessage(Messages messages, int level) {
		return messages.marked(defaultDuration() + level * durationPerLevel());
	}

}
