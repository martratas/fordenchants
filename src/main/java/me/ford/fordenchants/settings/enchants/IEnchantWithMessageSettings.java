package me.ford.fordenchants.settings.enchants;

import me.ford.fordenchants.settings.Messages;

public interface IEnchantWithMessageSettings extends IEnchantSettings {
	
	public default boolean sendMessage() {
		return getBoolean("send-message", true);
	}
	
	public static interface IEnchantWithMessageDependsOnLevelSettings extends IEnchantWithMessageSettings {
		
		public String getMessage(Messages messages, int level);
		
	}

}
