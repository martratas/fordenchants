package me.ford.fordenchants.settings.enchants.armor;

import me.ford.fordenchants.FordEnchants;
import me.ford.fordenchants.settings.enchants.EnchantSettings;
import me.ford.fordenchants.settings.enchants.IEnchantWithDurabilitySettings;

public class PhantomRepellentSettings extends EnchantSettings implements IEnchantWithDurabilitySettings {
	private static final String PATH = "enchants.phantom-repellent";
	
	public PhantomRepellentSettings(FordEnchants plugin) {
		super(plugin);
	}

	@Override
	public String getPath() {
		return PATH;
	}
	
	public boolean stopDamage() {
		return getBoolean("stop-damage", true);
	}

}
