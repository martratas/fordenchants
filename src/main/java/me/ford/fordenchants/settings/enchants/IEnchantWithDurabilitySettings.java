package me.ford.fordenchants.settings.enchants;

public interface IEnchantWithDurabilitySettings extends IEnchantSettings {
	
	public default int durabilityDefault() {
		return getInt("durability-default", 1);
	}
	
	public default int durabilityPerLevel() {
		return getInt("durability-per-level", 0);
	}
	
	public default int getDurability(int level) {
		return durabilityDefault() + level * durabilityPerLevel();
	}

}
