package me.ford.fordenchants.settings.enchants.weapon;

import me.ford.fordenchants.FordEnchants;
import me.ford.fordenchants.settings.enchants.EnchantSettings;
import me.ford.fordenchants.settings.enchants.IEnchantCanAffectPlayersSettings;
import me.ford.fordenchants.settings.enchants.IEnchantWithDurabilitySettings;

public class SummonWolfSettings extends EnchantSettings implements IEnchantCanAffectPlayersSettings, IEnchantWithDurabilitySettings {
	private static final String PATH = "enchants.summon-wolf";
	
	public SummonWolfSettings(FordEnchants plugin) {
		super(plugin);
	}

	@Override
	public String getPath() {
		return PATH;
	}
	
	public int wolfsDefault() {
		return getInt("wolfs-default", 0);
	}
	
	public int wolfsPerLevel() {
		return getInt("wolfs-per-level", 1);
	}
	
	public int wolfHelathDefault() {
		return getInt("wolf-health-default", 10);
	}
	
	public int wolfHealthPerLevel() {
		return getInt("wolf-health-per-level", 4);
	}
	
	public int wolfSpawnDelay() {
		return getInt("wolf-spawn-delay", 120);
	}
	
	public int wolfLifeTime() {
		return getInt("wolf-life-time", 60);
	}
	
	public boolean showSpawnMessage() {
		return getBoolean("show-spawn-msg", true);
	}
	
	public boolean showDespawnMessage() {
		return getBoolean("show-despawn-msg", true);
	}
	
	public String suffix() {
		return getString("suffix", "'s wolf");
	}
}
