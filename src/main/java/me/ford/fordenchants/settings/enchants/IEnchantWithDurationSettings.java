package me.ford.fordenchants.settings.enchants;

public interface IEnchantWithDurationSettings extends IEnchantSettings {

	public default double defaultDuration() {
		return getDouble("default-duration", 1.0);
	}
	
	public default double durationPerLevel() {
		return getDouble("duration-per-level", 0.5);
	}

}
