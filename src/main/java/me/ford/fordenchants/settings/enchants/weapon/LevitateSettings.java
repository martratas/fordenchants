package me.ford.fordenchants.settings.enchants.weapon;

import org.bukkit.entity.EntityType;

import me.ford.fordenchants.FordEnchants;
import me.ford.fordenchants.settings.Messages;
import me.ford.fordenchants.settings.enchants.EnchantSettings;
import me.ford.fordenchants.settings.enchants.IEnchantCanAffectPlayersSettings;
import me.ford.fordenchants.settings.enchants.IEnchantWithChanceSettings;
import me.ford.fordenchants.settings.enchants.IEnchantWithDurabilitySettings;
import me.ford.fordenchants.settings.enchants.IEnchantWithDurationSettings;
import me.ford.fordenchants.settings.enchants.IEnchantWithLevelSettings;
import me.ford.fordenchants.settings.enchants.IEnchantWithMessageSettings;

public class LevitateSettings extends EnchantSettings implements IEnchantWithChanceSettings, IEnchantWithDurationSettings, IEnchantWithLevelSettings, IEnchantWithMessageSettings, IEnchantCanAffectPlayersSettings, IEnchantWithDurabilitySettings {
	private static final String PATH = "enchants.levitate";
	
	public LevitateSettings(FordEnchants plugin) {
		super(plugin);
	}

	@Override
	public String getPath() {
		return PATH;
	}
	
	public int cooldown() {
		return getInt("cooldown", 120);
	}
	
	public double range() {
		return getDouble("range", 10.0);
	}
	
	public boolean isExempt(EntityType type) {
		return getStringList("exempt").contains(type.name()); // TODO - what if the list elements are not upper case
	}
	
	public String getMessage(Messages messages, int level, double duration) {
		return messages.levitated(level, duration);
	}

}
