package me.ford.fordenchants.settings.enchants.weapon;

import org.bukkit.entity.EntityType;

import me.ford.fordenchants.FordEnchants;
import me.ford.fordenchants.settings.Messages;
import me.ford.fordenchants.settings.enchants.EnchantSettings;
import me.ford.fordenchants.settings.enchants.IEnchantCanAffectPlayersSettings;
import me.ford.fordenchants.settings.enchants.IEnchantWithChanceSettings;
import me.ford.fordenchants.settings.enchants.IEnchantWithDurabilitySettings;
import me.ford.fordenchants.settings.enchants.IEnchantWithMessageSettings;

public class PullSettings extends EnchantSettings implements IEnchantWithChanceSettings, IEnchantWithMessageSettings, IEnchantCanAffectPlayersSettings, IEnchantWithDurabilitySettings {
	private static final String PATH = "enchants.pull";
	
	public PullSettings(FordEnchants plugin) {
		super(plugin);
	}

	@Override
	public String getPath() {
		return PATH;
	}
	
	public int cooldown() {
		return getInt("cooldown", 120);
	}
	
	public double defaultStrength() {
		return getDouble("default-strength", 0.5);
	}
	
	public double strengthPerLevel() {
		return getDouble("strength-per-level", 0.1);
	}
	
	public double range() {
		return getDouble("range", 10.0);
	}
	
	public boolean isExempt(EntityType type) {
		return getStringList("exempt").contains(type.name()); // TODO - what if the list elements are not upper case
	}
	
	public String getMessage(Messages messages, int level) {
		return messages.pulled(level);
	}

}
