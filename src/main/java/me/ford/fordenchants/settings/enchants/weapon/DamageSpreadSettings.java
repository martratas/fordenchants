package me.ford.fordenchants.settings.enchants.weapon;

import me.ford.fordenchants.FordEnchants;
import me.ford.fordenchants.settings.Messages;
import me.ford.fordenchants.settings.enchants.EnchantSettings;
import me.ford.fordenchants.settings.enchants.IEnchantCanAffectPlayersSettings;
import me.ford.fordenchants.settings.enchants.IEnchantWithChanceSettings;
import me.ford.fordenchants.settings.enchants.IEnchantWithDurabilitySettings;
import me.ford.fordenchants.settings.enchants.IEnchantWithMessageSettings;

public class DamageSpreadSettings extends EnchantSettings implements IEnchantWithChanceSettings, IEnchantWithMessageSettings, IEnchantCanAffectPlayersSettings, IEnchantWithDurabilitySettings {
	private static final String PATH = "enchants.damage-spread";
	
	public DamageSpreadSettings(FordEnchants plugin) {
		super(plugin);
	}

	@Override
	public String getPath() {
		return PATH;
	}
	
	public double connectionDistance() {
		return getDouble("connection-distance", 2.0);
	}
	
	public int connectionsDefault() {
		return getInt("connections-default", 0);
	}
	
	public int connectionsPerLevel() {
		return getInt("connections-per-level", 1);
	}
	
	public double connectionReduction() {
		return getDouble("connection-reduction", 0.8);
	}
	
	public boolean affectPlayers() {
		return getBoolean("affect-players", false);
	}
	
	public boolean affectSelf() {
		return getBoolean("affect-self", false);
	}
	
	public boolean showParticles() {
		return getBoolean("show-particles", true);
	}
	
	public int nrOfParticles() {
		return getInt("nr-of-particles", 50);
	}
	
	public boolean attackOwned() {
		return getBoolean("attack-owned", false);
	}
	
	public String getMessage(Messages messages, int nr) {
		return messages.damageSpread(nr);
	}

}
