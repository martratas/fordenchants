package me.ford.fordenchants.settings.enchants;

public interface IEnchantWithLevelSettings extends IEnchantSettings {

	public default int defaultLevel() {
		return getInt("default-level", 1);
	}
	
	public default int levelPerLevel() {
		return getInt("level-per-level", 1);
	}

}
