package me.ford.fordenchants.settings.enchants.bow;

import me.ford.fordenchants.FordEnchants;
import me.ford.fordenchants.settings.Messages;
import me.ford.fordenchants.settings.enchants.DisarmNeutralizeBaseSettings;

public class DisarmSettings extends DisarmNeutralizeBaseSettings {
	private static final String PATH = "enchants.disarm";
	
	public DisarmSettings(FordEnchants plugin) {
		super(plugin);
	}

	@Override
	public String getPath() {
		return PATH;
	}
	
	@Override
	public String getMessage(Messages messages, int level) {
		return messages.disarmed(level);
	}

}
