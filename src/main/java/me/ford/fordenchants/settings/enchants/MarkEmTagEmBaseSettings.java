package me.ford.fordenchants.settings.enchants;

import me.ford.fordenchants.FordEnchants;
import me.ford.fordenchants.settings.enchants.IEnchantWithMessageSettings.IEnchantWithMessageDependsOnLevelSettings;

public abstract class MarkEmTagEmBaseSettings extends EnchantSettings implements IEnchantWithChanceSettings, IEnchantWithMessageDependsOnLevelSettings, IEnchantCanAffectPlayersSettings, IEnchantWithDurationSettings, IEnchantWithDurabilitySettings {

	public MarkEmTagEmBaseSettings(FordEnchants plugin) {
		super(plugin);
	}

	public int cooldown() {
		return getInt("cooldown", 120);
	}
	
}
