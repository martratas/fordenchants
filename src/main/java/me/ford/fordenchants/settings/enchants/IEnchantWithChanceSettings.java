package me.ford.fordenchants.settings.enchants;

public interface IEnchantWithChanceSettings extends IEnchantSettings {

	public default double defaultChance() {
		return getDouble("default-chance", 0.5);
	}
	
	public default double chancePerLevel() {
		return getDouble("chance-per-level", 0.1);
	}

}
