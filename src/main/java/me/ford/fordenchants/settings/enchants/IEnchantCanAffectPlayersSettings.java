package me.ford.fordenchants.settings.enchants;

public interface IEnchantCanAffectPlayersSettings extends IEnchantSettings {
	
	public default boolean affectPlayers() {
		return getBoolean("affect-players", false);
	}

}
