package me.ford.fordenchants.settings.enchants.armor;

import me.ford.fordenchants.FordEnchants;
import me.ford.fordenchants.settings.enchants.EnchantSettings;
import me.ford.fordenchants.settings.enchants.IEnchantWithDurabilitySettings;

public class SpawnerEnhanceSettings extends EnchantSettings implements IEnchantWithDurabilitySettings {
	private static final String PATH = "enchants.spawner-enhance";
	
	public SpawnerEnhanceSettings(FordEnchants plugin) {
		super(plugin);
	}

	@Override
	public String getPath() {
		return PATH;
	}
	
	public int getNearbyMultiplier() {
		return getInt("nearby-multiplier", 1);
	}
	
	public int getMinDelayMultiplier() {
		return getInt("mindelay-multiplier", 2);
	}
	
	public int getMaxDelayMultiplier() {
		return getInt("maxdelay-multiplier", 4);
	}
	
}
