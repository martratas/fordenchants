package me.ford.fordenchants.settings.enchants;

import org.bukkit.configuration.ConfigurationSection;

import me.ford.fordenchants.FordEnchants;

public abstract class EnchantSettings implements IEnchantSettings {
	protected final FordEnchants FE;
	
	public EnchantSettings(FordEnchants plugin) {
		FE = plugin;
	}
	
	public abstract String getPath();
	
	@Override
	public final ConfigurationSection get() {
		return FE.getConfig().getConfigurationSection(getPath());
	}

}
