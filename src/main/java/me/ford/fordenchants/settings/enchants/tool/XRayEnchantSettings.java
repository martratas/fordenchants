package me.ford.fordenchants.settings.enchants.tool;

import me.ford.fordenchants.FordEnchants;
import me.ford.fordenchants.settings.enchants.EnchantSettings;
import me.ford.fordenchants.settings.enchants.IEnchantWithDurabilitySettings;

/**
 * XRayEnchantSettings
 */
public class XRayEnchantSettings extends EnchantSettings implements IEnchantWithDurabilitySettings {
	private static final String PATH = "enchants.x-ray";

    public XRayEnchantSettings(FordEnchants plugin) {
        super(plugin);
    }

    @Override
    public String getPath() {
        return PATH;
    }
	
	public int defaultDistance() {
		return getInt("default-distance", 0);
	}
	
	public int distancePerLevel() {
		return getInt("distance-per-level", 1);
	}
    
}