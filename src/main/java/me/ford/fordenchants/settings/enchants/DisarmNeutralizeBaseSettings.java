package me.ford.fordenchants.settings.enchants;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.EntityType;

import me.ford.fordenchants.FordEnchants;
import me.ford.fordenchants.settings.enchants.IEnchantWithMessageSettings.IEnchantWithMessageDependsOnLevelSettings;

public abstract class DisarmNeutralizeBaseSettings extends EnchantSettings implements IEnchantWithChanceSettings, IEnchantWithMessageDependsOnLevelSettings, IEnchantCanAffectPlayersSettings, IEnchantWithDurabilitySettings {
	
	public DisarmNeutralizeBaseSettings(FordEnchants plugin) {
		super(plugin);
	}
	
	public boolean throwOnGround() {
		return getBoolean("throw-on-ground", false);
	}
	
	public boolean putInInventoryForPlayers() {
		return getBoolean("put-in-inventory-for-players", true);
	}

	public int cooldown() {
		return getInt("cooldown", 120);
	}
	
	public double giveBackAfterDefault() {
		return getDouble("give-back-after.default-time", 1.0);
	}
	
	public double giveBackAfterPerLevel() {
		return getDouble("give-back-after.per-level-time", 0.1);
	}
	
	public double giveBackAfter(int level) {
		return giveBackAfterDefault() + level * giveBackAfterPerLevel();
	}
	
	public double chanceOfDroppingWhileDisarmedBase() {
		return getDouble("chance-to-drop-while-disarmed.BASE", 0.085);
	}
	
	public double chanceOfDroppingWhileDisarmed(EntityType type) {
		ConfigurationSection section = get().getConfigurationSection("chance-to-drop-while-disarmed");
		return chanceOfDroppingWhileDisarmedBase() + section.getDouble(type.name(), 0.0);
	}

}
