package me.ford.fordenchants.settings.enchants.crossbow;

import me.ford.fordenchants.FordEnchants;
import me.ford.fordenchants.settings.Messages;
import me.ford.fordenchants.settings.enchants.MarkEmTagEmBaseSettings;

public class TagEmSettings extends MarkEmTagEmBaseSettings {
	private static final String PATH = "enchants.tag-em";

	public TagEmSettings(FordEnchants plugin) {
		super(plugin);
	}

	@Override
	public String getPath() {
		return PATH;
	}

	@Override
	public String getMessage(Messages messages, int level) {
		return messages.tagged(defaultDuration() + level * durationPerLevel());
	}

}
