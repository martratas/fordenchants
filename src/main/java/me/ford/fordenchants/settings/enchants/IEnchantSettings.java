package me.ford.fordenchants.settings.enchants;

import java.util.List;

import org.bukkit.configuration.ConfigurationSection;

public interface IEnchantSettings {
	
	public ConfigurationSection get();
	
	public default String getString(String path, String def) {
		return get().getString(path, def);
	}
	
	public default boolean getBoolean(String path, boolean def) {
		return get().getBoolean(path, def);
	}
	
	public default int getInt(String path, int def) {
		return get().getInt(path, def);
	}
	
	public default double getDouble(String path, double def) {
		return get().getDouble(path, def);
	}
	
	public default List<String> getStringList(String path) {
		return get().getStringList(path);
	}
	
	public default int maxLevel() {
		return getInt("max-level", 1);
	}
	
	public default double chanceInTable() {
		return getDouble("chance-to-get-from-enchanting", 0.1);
	}
}
