package me.ford.fordenchants.settings.enchants.weapon;

import me.ford.fordenchants.FordEnchants;
import me.ford.fordenchants.settings.Messages;
import me.ford.fordenchants.settings.enchants.EnchantSettings;
import me.ford.fordenchants.settings.enchants.IEnchantCanAffectPlayersSettings;
import me.ford.fordenchants.settings.enchants.IEnchantWithChanceSettings;
import me.ford.fordenchants.settings.enchants.IEnchantWithDurabilitySettings;
import me.ford.fordenchants.settings.enchants.IEnchantWithDurationSettings;
import me.ford.fordenchants.settings.enchants.IEnchantWithLevelSettings;
import me.ford.fordenchants.settings.enchants.IEnchantWithMessageSettings;

public class WeakenSettings extends EnchantSettings implements IEnchantWithDurationSettings, IEnchantWithLevelSettings, IEnchantWithChanceSettings, IEnchantWithMessageSettings, IEnchantCanAffectPlayersSettings, IEnchantWithDurabilitySettings {
	private static final String PATH = "enchants.weaken";
	
	public WeakenSettings(FordEnchants plugin) {
		super(plugin);
	}

	@Override
	public String getPath() {
		return PATH;
	}

	public String getMessage(Messages messages, int level, double duration) {
		return messages.weakened(level, duration);
	}

}
