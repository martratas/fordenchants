package me.ford.fordenchants.settings.enchants.armor;

import me.ford.fordenchants.FordEnchants;
import me.ford.fordenchants.settings.Messages;
import me.ford.fordenchants.settings.enchants.EnchantSettings;

public class MagnetSettings extends EnchantSettings {
	private static final String PATH = "enchants.magnet";
	
	public MagnetSettings(FordEnchants plugin) {
		super(plugin);
	}

	@Override
	public String getPath() {
		return PATH;
	}
	
	public boolean dropLeftovers() {
		return getBoolean("drop-leftovers", true);
	}
	
	public double chanceToLoseDurabilityDefault() {
		return getDouble("chance-to-lose-durability-default", 0.9);
	}
	
	public double chanceToLoseDurabilityPerLevel() {
		return getDouble("chance-to-lose-durability-per-level", 0.1);
	}
	
	public double chanceToLoseDurability(int level) {
		return chanceToLoseDurabilityDefault() + level * chanceToLoseDurabilityPerLevel(); 
	}
	
	public boolean sendFullMessage() {
		return getBoolean("send-message-when-full", true);
	}
	
	public String getFullMessage(Messages messages) {
		return messages.full();
	}
	
	public boolean getFromInventory() {
		return getBoolean("get-from-inventory", true);
	}
	
	public boolean getFromKills() {
		return getBoolean("get-from-kills", true);
	}

}
