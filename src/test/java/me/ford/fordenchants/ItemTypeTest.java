package me.ford.fordenchants;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import be.seeseemelk.mockbukkit.MockBukkit;
import me.ford.fordenchants.utils.ItemUtils;

public class ItemTypeTest {

	@Before
	public void setUp() {
	    MockBukkit.mock();MockBukkit.load(FordEnchants.class);
	}

	@After
	public void tearDown() {
		MockBukkit.unload();
	}
	
	@Test
	public void helmet_is_helmet() {
		ItemStack item = new ItemStack(Material.CHAINMAIL_HELMET);
		Assert.assertTrue(ItemUtils.isHelmet(item));
	}
	
	@Test
	public void chestplate_is_chestplate() {
		ItemStack item = new ItemStack(Material.DIAMOND_CHESTPLATE);
		Assert.assertTrue(ItemUtils.isChestPlate(item));
	}
	
	@Test
	public void leggings_are_leggings() {
		ItemStack item = new ItemStack(Material.IRON_LEGGINGS);
		Assert.assertTrue(ItemUtils.isLeggings(item));
	}
	
	@Test
	public void boots_are_boots() {
		ItemStack item = new ItemStack(Material.LEATHER_BOOTS);
		Assert.assertTrue(ItemUtils.isBoots(item));
	}
	
	@Test
	public void helmet_is_armor() {
		ItemStack item = new ItemStack(Material.CHAINMAIL_HELMET);
		Assert.assertTrue(ItemUtils.isArmor(item));
	}
	
	@Test
	public void chestplate_is_armor() {
		ItemStack item = new ItemStack(Material.DIAMOND_CHESTPLATE);
		Assert.assertTrue(ItemUtils.isArmor(item));
	}
	
	@Test
	public void leggings_are_armor() {
		ItemStack item = new ItemStack(Material.IRON_LEGGINGS);
		Assert.assertTrue(ItemUtils.isArmor(item));
	}
	
	@Test
	public void boots_are_armor() {
		ItemStack item = new ItemStack(Material.LEATHER_BOOTS);
		Assert.assertTrue(ItemUtils.isArmor(item));
	}
	
	@Test
	public void sword_is_sword() {
		ItemStack item = new ItemStack(Material.IRON_SWORD);
		Assert.assertTrue(ItemUtils.isSword(item));
	}
	
	@Test
	public void axe_is_axe() {
		ItemStack item = new ItemStack(Material.STONE_AXE);
		Assert.assertTrue(ItemUtils.isAxe(item));
	}
	
	@Test
	public void bow_is_bow() {
		ItemStack item = new ItemStack(Material.BOW);
		Assert.assertTrue(ItemUtils.isBow(item));
	}
	
	@Test
	public void crossbow_is_crossbow() {
		ItemStack item = new ItemStack(Material.CROSSBOW);
		Assert.assertTrue(ItemUtils.isCrossbow(item));
	}
	
	@Test
	public void sword_is_weapon() {
		ItemStack item = new ItemStack(Material.STONE_SWORD);
		Assert.assertTrue(ItemUtils.isWeapon(item));
	}
	
	@Test
	public void axe_is_weapon() {
		ItemStack item = new ItemStack(Material.GOLDEN_AXE);
		Assert.assertTrue(ItemUtils.isWeapon(item));
	}

}
