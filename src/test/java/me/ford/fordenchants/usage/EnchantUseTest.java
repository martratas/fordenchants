package me.ford.fordenchants.usage;

// import java.util.ArrayList;
// import java.util.List;

import org.bukkit.Location;
import org.bukkit.Material;
// import org.bukkit.entity.Zombie;
// import org.bukkit.inventory.ItemStack;
import org.junit.After;
// import org.junit.Assert;
import org.junit.Before;
// import org.junit.Test;

import be.seeseemelk.mockbukkit.MockBukkit;
import be.seeseemelk.mockbukkit.ServerMock;
import be.seeseemelk.mockbukkit.WorldMock;
import be.seeseemelk.mockbukkit.entity.PlayerMock;
// import me.ford.fordenchants.FordEnchants;
// import me.ford.fordenchants.enchants.FordEnchant;
// import me.ford.fordenchants.settings.enchants.weapon.LeapSettings;
// import me.ford.fordenchants.utils.ItemUtils;

public class EnchantUseTest {
	// private FordEnchants plugin;
	private ServerMock server;
	private PlayerMock player;
	private WorldMock world;

	@Before
	public void setUp() {
	    server = MockBukkit.mock();
	    // plugin = MockBukkit.load(FordEnchants.class);
	    player = server.addPlayer("the_coolest_name");
	    world = new WorldMock(Material.DIRT, 4);
	    player.teleport(new Location(world, 0, 5, 0));
	}

	@After
	public void tearDown() {
	    MockBukkit.unload();
	}
	
	// @Test
	// public void interact_with_air() {
	// 	ItemStack item = new ItemStack(Material.DIAMOND_SWORD);
	// 	FordEnchant en = plugin.getEnchant("LEAP");
	// 	int level = 5;
	// 	ItemUtils.addEnchant(item, en, level, false);
	// 	player.getInventory().setItemInMainHand(item);
	// 	player.simulateInteractAir();
	// 	LeapSettings settings = (LeapSettings) en.getSettings();
	// 	double expected = settings.defaultVelocity() + level * settings.velocityPerLevel();
	// 	expected = expected * expected; // squared
	// 	double actual = player.getVelocity().lengthSquared();
	// 	double eps = 0.01; // is it big enough?
	// 	Assert.assertEquals(expected, actual, eps);
	// 	// I could test right click ? I think I added the mockbukkit method but I need to update the local .m2
	// }
}
