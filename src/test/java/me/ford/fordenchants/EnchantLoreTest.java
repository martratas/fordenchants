package me.ford.fordenchants;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import be.seeseemelk.mockbukkit.MockBukkit;
import be.seeseemelk.mockbukkit.inventory.meta.ItemMetaMock;
import me.ford.fordenchants.FordEnchants;
import me.ford.fordenchants.enchants.FordEnchant;
import me.ford.fordenchants.utils.ItemUtils;
import me.ford.fordenchants.utils.Utils;
import net.minecraft.server.v1_15_R1.IRegistry;

public class EnchantLoreTest {
	private FordEnchants plugin;
	private static boolean registered = false;
	
	public EnchantLoreTest() {
		if (!registered) { // otherwise enchantments are not registered
			Utils.enableRegistration();
		    IRegistry.ENCHANTMENT.getClass(); // load in enchantments
		    Utils.disableRegistration();
			registered = true;
		}
	}

	@Before
	public void setUp() {
	    MockBukkit.mock();
	    plugin = MockBukkit.load(FordEnchants.class);
	}

	@After
	public void tearDown() {
		MockBukkit.unload();
	}
	
	// Enchants (in the meta) are broken in the mock factory 
	public void enchantItem(ItemStack item, FordEnchant en, int expected) throws IllegalArgumentException {
		item = item.clone(); // clone
		ItemUtils.addEnchant(item, en, expected, false);
		ItemMetaMock meta = (ItemMetaMock) item.getItemMeta();
		if (en.canEnchantItem(item)) {
			meta.assertLore(en.getDisplayName() + " " + Utils.getRoman(expected));
			Assert.assertEquals(expected, ItemUtils.getLevel(item, en));
		} else {
			meta.assertHasNoLore();
			Assert.assertEquals(0, ItemUtils.getLevel(item, en));
		}		
	}
	
	@Test
	public void on_enchant_diamond_sword() {
		ItemStack item = new ItemStack(Material.DIAMOND_SWORD);
		int expected = 1;
		for (FordEnchant en : plugin.getEnchants()) {
			if (en.canEnchantItem(item)) enchantItem(item, en, expected);
		}
	}
	
	@Test
	public void on_enchant_iron_boots() {
		ItemStack item = new ItemStack(Material.IRON_BOOTS);
		int expected = 2;
		for (FordEnchant en : plugin.getEnchants()) {
			if (en.canEnchantItem(item)) enchantItem(item, en, expected);
		}
	}
	
	@Test
	public void on_enchant_bow() {
		ItemStack item = new ItemStack(Material.BOW);
		int expected = 5;
		for (FordEnchant en : plugin.getEnchants()) {
			if (en.canEnchantItem(item)) enchantItem(item, en, expected);
		}
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void on_enchant_diamond_sword_cannot() {
		ItemStack item = new ItemStack(Material.DIAMOND_SWORD);
		int expected = 1;
		for (FordEnchant en : plugin.getEnchants()) {
			if (!en.canEnchantItem(item)) enchantItem(item, en, expected);
		}		
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void on_enchant_iron_boots_cannot() {
		ItemStack item = new ItemStack(Material.IRON_BOOTS);
		int expected = 1;
		for (FordEnchant en : plugin.getEnchants()) {
			if (!en.canEnchantItem(item)) enchantItem(item, en, expected);
		}		
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void on_enchant_bow_cannot() {
		ItemStack item = new ItemStack(Material.BOW);
		int expected = 1;
		for (FordEnchant en : plugin.getEnchants()) {
			if (!en.canEnchantItem(item)) enchantItem(item, en, expected);
		}		
	}

}
