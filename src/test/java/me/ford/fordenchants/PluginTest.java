package me.ford.fordenchants;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import be.seeseemelk.mockbukkit.MockBukkit;
import me.ford.fordenchants.enchants.FordEnchant;

public class PluginTest {
	private FordEnchants plugin;

	@Before
	public void setUp() {
	    MockBukkit.mock();
	    plugin = MockBukkit.load(FordEnchants.class);
	}

	@After
	public void tearDown() {
	    MockBukkit.unload();
	}
	
	@Test
	public void enchant_names_exist() {
		Assert.assertNotNull(plugin.getEnchantNames());
		Assert.assertFalse(plugin.getEnchantNames().isEmpty());
	}
	
	@Test
	public void enchants_names_refer_to_enchant() {
		for (String name : plugin.getEnchantNames()) {
			Assert.assertNotNull(plugin.getEnchant(name));
		}
	}
	
	@Test
	public void enchants_exist() {
		Assert.assertNotNull(plugin.getEnchants());
		Assert.assertFalse(plugin.getEnchants().isEmpty());
	}
	
	@Test
	public void enchants_not_null() {
		for (FordEnchant ench : plugin.getEnchants()) {
			Assert.assertNotNull(ench);
		}
	}
	
	@Test
	public void random_provider_not_null() {
		Assert.assertNotNull(plugin.getRandomProvider());
	}
	
	@Test
	public void messages_not_null() {
		Assert.assertNotNull(plugin.getMessages());
	}
	
	@Test
	public void settings_not_null() {
		Assert.assertNotNull(plugin.getSettings());
	}

}
